package com.pascalwalter.service;

import com.pascalwalter.model.models.Patient;
import com.pascalwalter.model.models.SupervisionHour;
import com.pascalwalter.model.models.Supervisor;
import com.pascalwalter.model.query.AddSupervisionHourFormQuery;

import java.util.List;

public class AddSupervisionHourFormService {
    private final AddSupervisionHourFormQuery addSupervisionHourQuery;

    public AddSupervisionHourFormService() {
        this.addSupervisionHourQuery = new AddSupervisionHourFormQuery();
    }

    public List<Patient> listPatients()
    {
        return this.addSupervisionHourQuery.listPatients();
    }

    public List<Supervisor> listSupervisors()
    {
        return this.addSupervisionHourQuery.listSupervisors();
    }

    public SupervisionHour addSupervisionHour(SupervisionHour supervisionHour)
    {
        return addSupervisionHourQuery.addSupervisionHour(supervisionHour);
    }
}
