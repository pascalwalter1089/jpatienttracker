package com.pascalwalter.service;

import com.pascalwalter.model.models.Patient;
import com.pascalwalter.model.query.RemovePatientQuery;

public class RemovePatientService {
    RemovePatientQuery query = new RemovePatientQuery();
    public void removePatient(Patient patient) {
        query.removePatient(patient);
    }
}
