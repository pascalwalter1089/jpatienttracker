package com.pascalwalter.service;

import com.pascalwalter.model.models.Patient;
import com.pascalwalter.model.query.ListPatientsWithStatus;

import java.util.Collections;
import java.util.List;

public class ListPatientsWithStatusService {
    private ListPatientsWithStatus query;
    public ListPatientsWithStatusService() {
        this.query = new ListPatientsWithStatus();
    }

    public List<Patient> listPatientsWithStatus(int status) {
        if (!Collections.singletonList(Patient.PATIENT_STATES).contains(status)) {
            throw new RuntimeException("Status is invalid");
        }

        return this.query.listPatientsWithStatus(status);
    }
}
