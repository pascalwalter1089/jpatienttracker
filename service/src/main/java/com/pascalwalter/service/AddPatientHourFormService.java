package com.pascalwalter.service;

import com.pascalwalter.model.models.Patient;
import com.pascalwalter.model.models.PatientHour;
import com.pascalwalter.model.query.AddHoursForPatientFormQuery;
import com.pascalwalter.model.query.ListPatientsWithStatus;
import com.pascalwalter.model.query.MainFrameQueryImpl;

import java.util.List;

public class AddPatientHourFormService {
    private final AddHoursForPatientFormQuery addHoursForPatientQuery;
    MainFrameQueryImpl mainFrameQueryImpl;
    private ListPatientsWithStatus listPatientsWithStatusQuery;

    public AddPatientHourFormService() {
        mainFrameQueryImpl = new MainFrameQueryImpl();
        addHoursForPatientQuery = new AddHoursForPatientFormQuery();
        listPatientsWithStatusQuery = new ListPatientsWithStatus();
    }
    public List<Patient> listLaufendePatients() {
        return listPatientsWithStatusQuery.listPatientsWithStatus(Patient.PATIENT_STATE_LAUFEND);
    }

    public PatientHour addHoursForPatient(PatientHour patientHour) {
        return addHoursForPatientQuery.addPatientHour(patientHour);
    }
}
