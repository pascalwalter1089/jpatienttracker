package com.pascalwalter.service;

import com.pascalwalter.model.models.Patient;
import com.pascalwalter.model.models.PatientHour;

import java.util.ArrayList;
import java.util.List;

public class PatientHourCountService {

    public static float calculatePatientHourCount(Patient patient) {
        List<PatientHour> hours = patient.getHours().stream().toList();

        List<PatientHour> probatorikHours =
                hours.stream().filter(i -> i.getType() == PatientHour.PATIENT_HOUR_TYPE_PROBATORIK).toList();

        float probatorikHoursCount =
                probatorikHours.size() <= 3 ? probatorikHours.size() : 3 + (probatorikHours.size() - 3) * 0.6f;

        return (hours.size() - probatorikHours.size()) + probatorikHoursCount;
    }
}
