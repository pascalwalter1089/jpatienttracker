package com.pascalwalter.service.folder;

import com.pascalwalter.utils.StringConstants;

import java.io.File;
import java.io.IOException;

public class RootApplictaionDirectoryHandler {

    public static String APPLICATION_ROOT_DIR = ".patienttracker";

    public static String getPathToApplicationRootDir() {
        return String.format("%s/%s", System.getProperty("user.home"), RootApplictaionDirectoryHandler.APPLICATION_ROOT_DIR);
    }

    public static boolean doesApplicationDataRootDirExsists() {
        return new File(getPathToApplicationRootDir()).exists();
    }

    private static boolean createDirectory(String path)  {
        return new File(path).mkdir();
    }

    public static void createApplicationDataRootDir() throws FolderNotCreatedException {
        boolean result = createDirectory(getPathToApplicationRootDir());

        if (!result) {
            throw new FolderNotCreatedException();
        }
    }

    public static boolean doesApplicationDatabaseExist() {
        return new File(String.format("%s/%s.mv.db", getPathToApplicationRootDir(), StringConstants.DATABASE_NAME)).exists();
    }

    public static class FolderNotCreatedException extends Exception {
        public FolderNotCreatedException() {
            super("Root folder could not be created!");
        }
    }


    private static class ApplicationDatabaseNotCreatedException extends IOException {
        public ApplicationDatabaseNotCreatedException(String databaseName, Throwable cause) {super(String.format("Application %s database could not get created!", databaseName), cause);}
    }
}
