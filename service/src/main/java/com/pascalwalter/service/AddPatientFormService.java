package com.pascalwalter.service;

import com.pascalwalter.model.models.Patient;
import com.pascalwalter.model.query.AddPatientFormQuery;

public class AddPatientFormService {
    AddPatientFormQuery query = new AddPatientFormQuery();
    public void addPatient(Patient patient) {
        query.addPatient(patient);
    }
}
