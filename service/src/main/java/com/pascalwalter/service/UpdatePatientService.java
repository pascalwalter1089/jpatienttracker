package com.pascalwalter.service;

import com.pascalwalter.model.models.Patient;
import com.pascalwalter.model.query.UpdatePatientQuery;

public class UpdatePatientService {
    private final UpdatePatientQuery updatePatientQuery;

    public UpdatePatientService() {
        this.updatePatientQuery = new UpdatePatientQuery();
    }

    public Patient updatePatient(Patient patient) {
        return this.updatePatientQuery.updatePatient(patient);
    }
}
