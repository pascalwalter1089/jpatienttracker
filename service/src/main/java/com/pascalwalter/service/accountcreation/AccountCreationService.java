package com.pascalwalter.service.accountcreation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AccountCreationService {

    public static final String PASSWORD_NEEDS_TO_CONTAIN_AT_LEAST_ONE_DIGIT_CHARACTER = "Password needs to contain at least one digit character";
    public static final String PASSWORD_NEEDS_TO_CONTAIN_LOWER_CHARACTERS = "Password needs to contain lower characters";
    public static final String PASSWORD_NEEDS_TO_CONTAIN_UPPERCASE_CHARACTERS = "Password needs to contain uppercase characters";
    public static final String PASSWORD_NEEDS_TO_CONTAIN_SPECIAL_CHARACTERS = "Password needs to contain special characters";
    public static final String PASSWORD_TO_SHORT = "Password to short";
    public static final String PASSWORDS_ARE_DIFFERENT = "Passwords are different";
    public static final String USERNAME_MUST_BE_LONGER_THEN_2_CHARACTERS = "Username must be longer then 2 characters";
    public static final String USERNAME_CANNOT_CONTAIN_SPECIAL_CHARACTERS_OR_WHITE_SPACES = "Username cannot contain Special characters or white spaces";

    public boolean validatePassword(char[] passwordOne, char[] passwordTwo) throws Exception {
        if (passwordOne.length != passwordTwo.length) {
            throw new Exception(PASSWORDS_ARE_DIFFERENT);
        }

        if (passwordOne.length < 14) {
            throw new Exception(PASSWORD_TO_SHORT);
        }

        StringBuilder passwordOneStringBuilder = new StringBuilder();
        for (char s : passwordOne) {
            passwordOneStringBuilder.append(s);
        }

        String passwordOneString = passwordOneStringBuilder.toString();
        String specialCharacters = "[-/@#$%^&_+=()!\\\\]";
        Pattern sepcialCharacterPattern = Pattern.compile(specialCharacters);
        Matcher matcher = sepcialCharacterPattern.matcher(passwordOneString);
        if (!matcher.find()) {
            throw new Exception(PASSWORD_NEEDS_TO_CONTAIN_SPECIAL_CHARACTERS);
        }


        if (passwordOneString.chars().noneMatch(Character::isUpperCase)) {
            throw new Exception(PASSWORD_NEEDS_TO_CONTAIN_UPPERCASE_CHARACTERS);
        }

        if (passwordOneString.chars().noneMatch(Character::isLowerCase)) {
            throw new Exception(PASSWORD_NEEDS_TO_CONTAIN_LOWER_CHARACTERS);
        }

        if (passwordOneString.chars().noneMatch(Character::isDigit)) {
            throw new Exception(PASSWORD_NEEDS_TO_CONTAIN_AT_LEAST_ONE_DIGIT_CHARACTER);
        }

        return true;
    }

    public boolean validateUserName(String userName) throws Exception {
        userName = userName.strip();
        if (userName.length() < 2) {
            throw new Exception(USERNAME_MUST_BE_LONGER_THEN_2_CHARACTERS);
        }

        Pattern whiteSpacePattern = Pattern.compile( "[-/@#$%^&_+=()!\\\\\s]");
        Matcher matcher = whiteSpacePattern.matcher(userName);
        if (matcher.find()) {
            throw new Exception(USERNAME_CANNOT_CONTAIN_SPECIAL_CHARACTERS_OR_WHITE_SPACES);
        }

        return true;
    }
}
