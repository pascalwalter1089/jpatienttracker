package com.pascalwalter.service;

import com.pascalwalter.model.query.ListSupervisionHoursByTypeInterface;
import com.pascalwalter.model.models.Patient;
import com.pascalwalter.model.models.SupervisionHour;
import com.pascalwalter.model.query.ListSupervisionHoursByTypeImpl;
import com.pascalwalter.model.query.MainFrameQueryImpl;
import com.pascalwalter.model.query.MainFrameQueryInterface;
import com.pascalwalter.utils.MathUtils;

import java.util.List;

public class GeneralPatientStatisticsService {
    //TODO: Make values for hours needed configureable!
    MainFrameQueryInterface mainFrameQuery;
    ListSupervisionHoursByTypeInterface supervisionHoursByTypeQuery;
    private List<Patient> patientHours;
    private double soloSupervisionHoursCount;
    private double groupSupervisionHoursCount;

    public GeneralPatientStatisticsService(ListSupervisionHoursByTypeInterface supervisionHoursByTypeQuery, MainFrameQueryInterface mainFrameQuery)
    {
        this.supervisionHoursByTypeQuery = supervisionHoursByTypeQuery;
        this.mainFrameQuery = mainFrameQuery;
    }

    public GeneralPatientStatisticsService()
    {
        mainFrameQuery = new MainFrameQueryImpl();
        supervisionHoursByTypeQuery = new ListSupervisionHoursByTypeImpl();
    }

    public void initValues()
    {
        this.patientHours = mainFrameQuery.listAllPatients();
        this.soloSupervisionHoursCount = getSupervisionHoursCountByType(SupervisionHour.SUPERVISION_HOUR_TYPE_EINZEL);
        this.groupSupervisionHoursCount = getSupervisionHoursCountByType(SupervisionHour.SUPERVISION_HOUR_TYPE_GROUP);
    }

    private double getSupervisionHoursCountByType(int type)
    {
        return supervisionHoursByTypeQuery.listSupervisionHoursByType(type)
                .stream()
                .mapToDouble(SupervisionHour::getDuration)
                .sum();
    }

    private double getGeneralGeleisteteStunden() {
        double patientHours = 0;
        for (Patient patient: this.patientHours) {
            patientHours += PatientHourCountService.calculatePatientHourCount(patient);
        }

        return MathUtils.round(patientHours, 1);
    }
    public double getGeleisteteStunden()
    {
        return getGeneralGeleisteteStunden();
    }

    public double getZuleistendeStunden()
    {
        return 600 - getGeneralGeleisteteStunden();
    }

    public double getEinzelSupervisionHour()
    {
        return this.soloSupervisionHoursCount;
    }

    public double getZuleistendeEinzelSupervisionHours()
    {
        return 50 - this.soloSupervisionHoursCount;
    }

    public double getGruppenSupervisionHour()
    {
        return this.groupSupervisionHoursCount;
    }

    public double getZuleistendeGruppenSupervisionHour()
    {
        return 100 - this.groupSupervisionHoursCount;
    }
}
