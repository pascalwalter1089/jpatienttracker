package com.pascalwalter.service;

import com.pascalwalter.model.DatabaseHandler;
import com.pascalwalter.service.folder.RootApplictaionDirectoryHandler;
import com.pascalwalter.utils.StringConstants;

import java.util.HashMap;
import java.util.Map;

public class InitializeAppService {
    public static final String HIBERNATE_CONNECTION_USER = "hibernate.connection.user";
    public static final String HIBERNATE_CONNECTION_PASSWORD = "hibernate.connection.password";

    public static void initializeApp() throws RootApplictaionDirectoryHandler.FolderNotCreatedException {

        if (!RootApplictaionDirectoryHandler.doesApplicationDataRootDirExsists()) {
            RootApplictaionDirectoryHandler.createApplicationDataRootDir();
        }
    }

    public static void initializeDatabase() throws Exception {
        // TODO: Login to database only when the password was past etc.. Will come with account creation
        HashMap<String, String> databaseSettings =  new HashMap<>();
        databaseSettings.put(
                "javax.persistence.jdbc.url",
                String.format(
                        "jdbc:h2:file:%s/%s",
                        RootApplictaionDirectoryHandler.getPathToApplicationRootDir(),
                        StringConstants.DATABASE_NAME
                )
        );
        DatabaseHandler.INSTANCE.init(databaseSettings);
    }

    public static void initializeDatabase(Map<String, String> settings) {
        settings.put(
                "javax.persistence.jdbc.url",
                String.format(
                        "jdbc:h2:file:%s/%s",
                        RootApplictaionDirectoryHandler.getPathToApplicationRootDir(),
                        StringConstants.DATABASE_NAME
                )
        );
        DatabaseHandler.INSTANCE.init(settings);
    }
}
