package com.pascalwalter.service;

import com.pascalwalter.model.models.Patient;
import com.pascalwalter.model.models.Supervisor;
import com.pascalwalter.model.query.MainFrameQueryImpl;

import java.util.List;

public class MainFrameService {
    MainFrameQueryImpl query;

    public MainFrameService() {
        query = new MainFrameQueryImpl();
    }

    public List<Patient> listAllPatients() {
        return query.listAllPatients();
    }

    public List<Supervisor> listAllSupervisors() {
        return query.listAllSupervisors();
    }

}
