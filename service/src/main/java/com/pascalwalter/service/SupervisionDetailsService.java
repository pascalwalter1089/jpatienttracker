package com.pascalwalter.service;

import com.pascalwalter.model.models.SupervisionHour;
import com.pascalwalter.model.models.Supervisor;
import com.pascalwalter.model.query.ListSupervisionHoursByTypeAndSupervisorImpl;
import com.pascalwalter.model.query.ListSupervisionHoursByTypeAndSupervisorQueryInterface;

public class SupervisionDetailsService {

    private final ListSupervisionHoursByTypeAndSupervisorQueryInterface query;

    public SupervisionDetailsService() {
        query = new ListSupervisionHoursByTypeAndSupervisorImpl();
    }

    public SupervisionDetailsService(ListSupervisionHoursByTypeAndSupervisorQueryInterface query) {
        this.query = query;
    }

    public double getCountOfGroupSupervisionHours(Supervisor supervisor)
    {
        return query.listHoursByTypeAndSupervisor(supervisor, SupervisionHour.SUPERVISION_HOUR_TYPE_GROUP).stream().mapToDouble(SupervisionHour::getDuration).sum();
    }

    public double getCountOfSoloSupervisionHours(Supervisor supervisor)
    {
        return query.listHoursByTypeAndSupervisor(supervisor, SupervisionHour.SUPERVISION_HOUR_TYPE_EINZEL).stream().mapToDouble(SupervisionHour::getDuration).sum();
    }
}
