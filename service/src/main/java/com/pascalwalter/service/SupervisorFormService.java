package com.pascalwalter.service;

import com.pascalwalter.model.models.Supervisor;
import com.pascalwalter.model.query.AddSupervisorFormQuery;

public class SupervisorFormService {

    private final AddSupervisorFormQuery addSupervisorQuery;

    public SupervisorFormService() {
        addSupervisorQuery = new AddSupervisorFormQuery();
    }
    public Supervisor addSupervisor(Supervisor supervisor) {
        return addSupervisorQuery.addSupervisor(supervisor);
    }
}
