package com.pascalwalter.service;

import com.pascalwalter.model.models.Patient;
import com.pascalwalter.model.models.PatientHour;
import com.pascalwalter.model.models.SupervisionHour;
import com.pascalwalter.utils.MathUtils;

public class PatientDetailsService {
    public static int calculateTotalBezugspersonenHoursForPatient(Patient patient) {
         return (int) patient
                .getHours()
                .stream()
                .filter(i -> i.getRecipient().equals(PatientHour.PatientHourRecipient.BEZUGSPERSON.toString()))
                .count();
    }

    public static double calculateTotalTherapyHours(Patient patient) {
        return MathUtils.round(PatientHourCountService.calculatePatientHourCount(patient), 1);
    }

    public static double calculateTotalTherapyHoursForPatient(Patient patient) {
        return patient
                .getHours()
                .stream()
                .filter(i -> i.getRecipient().equals(PatientHour.PatientHourRecipient.PATIENT.toString()))
                .count();
    }

    public static double calculateTotalSupervisionHoursForPatient(Patient patient) {
        return patient.getSupervisionHours().stream().mapToDouble(SupervisionHour::getDuration).sum();
    }
}
