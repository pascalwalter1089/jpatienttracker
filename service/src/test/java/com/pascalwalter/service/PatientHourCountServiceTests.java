package com.pascalwalter.service;

import com.pascalwalter.model.models.Patient;
import com.pascalwalter.model.models.PatientHour;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PatientHourCountServiceTests {

    @Test
    public void testCountWithLessThenThreeProbatorik() {
        Patient p = new Patient("JUnitTestName");
        PatientHour ph = new PatientHour(p, PatientHour.PATIENT_HOUR_TYPE_PROBATORIK, PatientHour.PatientHourRecipient.PATIENT, new Date(System.currentTimeMillis()));
        HashSet<PatientHour> set = new HashSet<>();
        set.add(ph);
        p.setHours(set);
        assertEquals(1f, PatientHourCountService.calculatePatientHourCount(p));
    }

    @Test
    public void testCountWithThreeProbatorik() {
        Patient p = new Patient("JUnitTestName");
        PatientHour ph1 = new PatientHour(p, PatientHour.PATIENT_HOUR_TYPE_PROBATORIK, PatientHour.PatientHourRecipient.PATIENT, new Date(System.currentTimeMillis()));
        PatientHour ph2 = new PatientHour(p, PatientHour.PATIENT_HOUR_TYPE_PROBATORIK, PatientHour.PatientHourRecipient.PATIENT, new Date(System.currentTimeMillis()));
        PatientHour ph3 = new PatientHour(p, PatientHour.PATIENT_HOUR_TYPE_PROBATORIK, PatientHour.PatientHourRecipient.PATIENT, new Date(System.currentTimeMillis()));
        HashSet<PatientHour> set = new HashSet<>();
        set.add(ph1);
        set.add(ph2);
        set.add(ph3);
        p.setHours(set);
        assertEquals(3f, PatientHourCountService.calculatePatientHourCount(p));
    }

    @Test
    public void testCountWithFourProbatorik() {
        Patient p = new Patient("JUnitTestName");
        PatientHour ph1 = new PatientHour(p, PatientHour.PATIENT_HOUR_TYPE_PROBATORIK, PatientHour.PatientHourRecipient.PATIENT, new Date(System.currentTimeMillis()));
        PatientHour ph2 = new PatientHour(p, PatientHour.PATIENT_HOUR_TYPE_PROBATORIK, PatientHour.PatientHourRecipient.PATIENT, new Date(System.currentTimeMillis()));
        PatientHour ph3 = new PatientHour(p, PatientHour.PATIENT_HOUR_TYPE_PROBATORIK, PatientHour.PatientHourRecipient.PATIENT, new Date(System.currentTimeMillis()));
        PatientHour ph4 = new PatientHour(p, PatientHour.PATIENT_HOUR_TYPE_PROBATORIK, PatientHour.PatientHourRecipient.PATIENT, new Date(System.currentTimeMillis()));
        HashSet<PatientHour> set = new HashSet<>();
        set.add(ph1);
        set.add(ph2);
        set.add(ph3);
        set.add(ph4);
        p.setHours(set);
        assertEquals(3.6f, PatientHourCountService.calculatePatientHourCount(p));
    }

    @Test
    public void testCountWithFourProbatorikPlusOther() {
        Patient p = new Patient("JUnitTestName");
        PatientHour ph1 = new PatientHour(p, PatientHour.PATIENT_HOUR_TYPE_PROBATORIK, PatientHour.PatientHourRecipient.PATIENT, new Date(System.currentTimeMillis()));
        PatientHour ph2 = new PatientHour(p, PatientHour.PATIENT_HOUR_TYPE_PROBATORIK, PatientHour.PatientHourRecipient.PATIENT, new Date(System.currentTimeMillis()));
        PatientHour ph3 = new PatientHour(p, PatientHour.PATIENT_HOUR_TYPE_PROBATORIK, PatientHour.PatientHourRecipient.PATIENT, new Date(System.currentTimeMillis()));
        PatientHour ph4 = new PatientHour(p, PatientHour.PATIENT_HOUR_TYPE_PROBATORIK, PatientHour.PatientHourRecipient.PATIENT, new Date(System.currentTimeMillis()));
        PatientHour ph5 = new PatientHour(p, PatientHour.PATIENT_HOUR_TYPE_LZT, PatientHour.PatientHourRecipient.PATIENT, new Date(System.currentTimeMillis()));
        HashSet<PatientHour> set = new HashSet<>();
        set.add(ph1);
        set.add(ph2);
        set.add(ph3);
        set.add(ph4);
        set.add(ph5);
        p.setHours(set);
        assertEquals(4.6f, PatientHourCountService.calculatePatientHourCount(p));
    }
}
