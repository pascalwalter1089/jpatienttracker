package com.pascalwalter.service;

import com.pascalwalter.model.models.Patient;
import com.pascalwalter.model.models.PatientHour;
import com.pascalwalter.model.models.SupervisionHour;
import com.pascalwalter.model.models.Supervisor;
import static org.junit.jupiter.api.Assertions.assertEquals;
import com.pascalwalter.model.query.MainFrameQueryInterface;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GeneralPatientStatisticsServiceTest {

    private GeneralPatientStatisticsService generalPatientStatisticsService;

    @BeforeEach
    public void setUp()
    {
        Patient patient1 = new Patient("JUNIT-PatientOneHash");
        Patient patient2 = new Patient("JUNIT-PatientTwoHash");
        Patient patient3 = new Patient("JUNIT-PatientThreeHash");

        PatientHour patientHour1 = new PatientHour(patient1, PatientHour.PATIENT_HOUR_TYPE_PROBATORIK, PatientHour.PatientHourRecipient.PATIENT, new Date(System.currentTimeMillis()));
        PatientHour patientHour2 = new PatientHour(patient1, PatientHour.PATIENT_HOUR_TYPE_PROBATORIK, PatientHour.PatientHourRecipient.PATIENT, new Date(System.currentTimeMillis()));
        PatientHour patientHour3 = new PatientHour(patient1, PatientHour.PATIENT_HOUR_TYPE_PROBATORIK, PatientHour.PatientHourRecipient.PATIENT, new Date(System.currentTimeMillis()));
        PatientHour patientHour4 = new PatientHour(patient1, PatientHour.PATIENT_HOUR_TYPE_PROBATORIK, PatientHour.PatientHourRecipient.PATIENT, new Date(System.currentTimeMillis()));
        HashSet<PatientHour> h = new HashSet<>(4);
        h.add(patientHour1);
        h.add(patientHour2);
        h.add(patientHour3);
        h.add(patientHour4);
        patient1.setHours(h);

        PatientHour patientHour5 = new PatientHour(patient2, PatientHour.PATIENT_HOUR_TYPE_KZT1, PatientHour.PatientHourRecipient.PATIENT, new Date(System.currentTimeMillis()));
        PatientHour patientHour6 = new PatientHour(patient2, PatientHour.PATIENT_HOUR_TYPE_KZT1, PatientHour.PatientHourRecipient.PATIENT, new Date(System.currentTimeMillis()));
        h = new HashSet<>(2);
        h.add(patientHour5);
        h.add(patientHour6);
        patient2.setHours(h);

        PatientHour patientHour7 = new PatientHour(patient3, PatientHour.PATIENT_HOUR_TYPE_KZT1, PatientHour.PatientHourRecipient.PATIENT, new Date(System.currentTimeMillis()));
        PatientHour patientHour8 = new PatientHour(patient3, PatientHour.PATIENT_HOUR_TYPE_KZT1, PatientHour.PatientHourRecipient.PATIENT, new Date(System.currentTimeMillis()));
        h = new HashSet<>(2);
        h.add(patientHour7);
        h.add(patientHour8);
        patient3.setHours(h);

        Supervisor supervisor1 = new Supervisor(1, "JUnit-SupervisorOneName");
        Supervisor supervisor2 = new Supervisor(2, "JUnit-SupervisorTwoName");

        SupervisionHour sh1 = new SupervisionHour(1., Set.of(patient1, patient2), supervisor1, SupervisionHour.SUPERVISION_HOUR_TYPE_EINZEL);
        SupervisionHour sh2 = new SupervisionHour(1., Set.of(patient1, patient2), supervisor1, SupervisionHour.SUPERVISION_HOUR_TYPE_EINZEL);
        SupervisionHour sh3 = new SupervisionHour(1., Set.of(patient1, patient2), supervisor1, SupervisionHour.SUPERVISION_HOUR_TYPE_GROUP);
        SupervisionHour sh4 = new SupervisionHour(1., Set.of(patient1, patient2), supervisor1, SupervisionHour.SUPERVISION_HOUR_TYPE_GROUP);

        generalPatientStatisticsService = new GeneralPatientStatisticsService(supervisionHoursType -> switch (supervisionHoursType) {
            case SupervisionHour.SUPERVISION_HOUR_TYPE_EINZEL:
                yield List.of(sh1, sh2);
            case SupervisionHour.SUPERVISION_HOUR_TYPE_GROUP:
                yield List.of(sh3, sh4);
            default:
                throw new IllegalStateException("Unexpected value: " + supervisionHoursType);
        },
                new MainFrameQueryInterface() {
                    @Override
                    public List<Patient> listAllPatients() {
                        return List.of(patient1, patient2, patient3);
                    }

                    @Override
                    public List<Supervisor> listAllSupervisors() {
                        return List.of(supervisor1, supervisor2);
                    }
                });
    }

    @Test
    public void testGetGeleisteteStunden() {
        generalPatientStatisticsService.initValues();
        assertEquals(7.6, generalPatientStatisticsService.getGeleisteteStunden());
        assertEquals(592.4, generalPatientStatisticsService.getZuleistendeStunden());
    }

    @Test
    public void testSupervisionStunden() {
        generalPatientStatisticsService.initValues();
        assertEquals(2, generalPatientStatisticsService.getEinzelSupervisionHour());
        assertEquals(48., generalPatientStatisticsService.getZuleistendeEinzelSupervisionHours());
        assertEquals(2, generalPatientStatisticsService.getGruppenSupervisionHour());
        assertEquals(98., generalPatientStatisticsService.getZuleistendeGruppenSupervisionHour());
    }
}
