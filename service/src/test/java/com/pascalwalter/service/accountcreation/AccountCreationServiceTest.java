package com.pascalwalter.service.accountcreation;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class AccountCreationServiceTest {

    static Stream<Arguments> invalidPasswordProvider() {
        return Stream.of(
                arguments("ValidPassword!1", "ValidPassword!2", AccountCreationService.PASSWORDS_ARE_DIFFERENT),
                arguments("NoNumberInPas!", "NoNumberInPas!", AccountCreationService.PASSWORD_NEEDS_TO_CONTAIN_AT_LEAST_ONE_DIGIT_CHARACTER),
                arguments("NoSpecialChar1", "NoSpecialChar1", AccountCreationService.PASSWORD_NEEDS_TO_CONTAIN_SPECIAL_CHARACTERS),
                arguments("nouppercasechar!1", "nouppercasechar!1", AccountCreationService.PASSWORD_NEEDS_TO_CONTAIN_UPPERCASE_CHARACTERS),
                arguments("NOLOWERCASECHAR!1", "NOLOWERCASECHAR!1", AccountCreationService.PASSWORD_NEEDS_TO_CONTAIN_LOWER_CHARACTERS),
                arguments("toShort", "toShort", AccountCreationService.PASSWORD_TO_SHORT)
        );
    }

    public static Stream<Arguments> invalidUserNameProvider() {
        return Stream.of(
                arguments("User name", AccountCreationService.USERNAME_CANNOT_CONTAIN_SPECIAL_CHARACTERS_OR_WHITE_SPACES),
                arguments("User&name", AccountCreationService.USERNAME_CANNOT_CONTAIN_SPECIAL_CHARACTERS_OR_WHITE_SPACES),
                arguments("1", AccountCreationService.USERNAME_MUST_BE_LONGER_THEN_2_CHARACTERS)
        );
    }

    @Test
    public void testValidatePassword() {
        AccountCreationService accountCreationService = new AccountCreationService();

        try {
            assertTrue(accountCreationService.validatePassword("ValidPassword!2".toCharArray(), "ValidPassword!2".toCharArray()));
        } catch (Exception e) {
            assertEquals(true, false);
        }
    }

    @ParameterizedTest
    @MethodSource("invalidPasswordProvider")
    public void testInvalidPassword(String passwordOne, String passwordTwo, String expectedMessage ) {
        AccountCreationService accountCreationService = new AccountCreationService();
        try {
            accountCreationService.validatePassword(passwordOne.toCharArray(), passwordTwo.toCharArray());
        } catch (Exception e) {
            assertEquals(expectedMessage, e.getMessage());
        }
    }

    @Test
    public void testValidUserName() {
        AccountCreationService accountCreationService = new AccountCreationService();
        try {
            assertTrue(accountCreationService.validateUserName("ValidUserName"));
        } catch (Exception e) {
            assertTrue(false);
        }
    }

    @ParameterizedTest
    @MethodSource("invalidUserNameProvider")
    public void testInvalidUserName(String userName, String expectedExceptionMessage) {
        AccountCreationService accountCreationService = new AccountCreationService();
        try {
            accountCreationService.validateUserName(userName);
        } catch (Exception e) {
            assertEquals(expectedExceptionMessage, e.getMessage());
        }
    }
}
