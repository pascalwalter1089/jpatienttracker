package com.pascalwalter.ui;

import com.pascalwalter.service.InitializeAppService;
import com.pascalwalter.service.folder.RootApplictaionDirectoryHandler;

import javax.swing.*;

public class App {
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException|InstantiationException|IllegalAccessException|UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        try {
            InitializeAppService.initializeApp();
        } catch (RootApplictaionDirectoryHandler.FolderNotCreatedException e) {
            throw new RuntimeException(e);
        }

        SwingUtilities.invokeLater(MainFrame::new);
        // TODO: User profile
        // TODO: Login or new Profile
    }
}
