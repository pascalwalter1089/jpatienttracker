package com.pascalwalter.ui.table;

import com.pascalwalter.model.models.Supervisor;
import com.pascalwalter.ui.dialog.SupervisorDetails;
import com.pascalwalter.ui.table.tableModel.UpdateableTableModel;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class SupervisionTable extends TablePanel {
    public SupervisionTable(UpdateableTableModel tableModel) {
        super(tableModel);
        initializeActionListener();
    }

    private void initializeActionListener() {
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                Point point = mouseEvent.getPoint();
                int row = SupervisionTable.this.table.rowAtPoint(point);
                if (mouseEvent.getClickCount() == 2 && row != -1) {
                    SupervisorDetails details = new SupervisorDetails(SupervisionTable.this, (Supervisor) SupervisionTable.this.tableModel.getRowItem(row));
                    details.setVisible(true);
                }
            }
        });
    }


}
