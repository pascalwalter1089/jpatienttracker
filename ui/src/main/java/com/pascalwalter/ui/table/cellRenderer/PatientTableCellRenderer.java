package com.pascalwalter.ui.table.cellRenderer;

import com.pascalwalter.ui.table.tableModel.PatientTableModel;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

public class PatientTableCellRenderer extends DefaultTableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        if (!isSelected) {
            PatientTableModel tm = (PatientTableModel) table.getModel();
            c.setBackground(tm.getBackgroundForPatient(row));
        }
        return c;
    }
}
