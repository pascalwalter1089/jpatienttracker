package com.pascalwalter.ui.table.tableModel;

import com.pascalwalter.model.models.PatientHour;
import com.pascalwalter.utils.StringConstants;

import java.util.List;

public class DetailedPatientHoursTableModel extends UpdateableTableModel {

    public DetailedPatientHoursTableModel(List<PatientHour> models) {
        hourList = models;
    }

    private final String[] columnNames = {
            StringConstants.PATIENT_DETAIL_TABLE_DATE,
            StringConstants.PATIENT_DETAIL_TABLE_TYPE,
            StringConstants.PATIENT_DETAIL_TABLE_DURATION,
            StringConstants.PATIENT_DETAIL_TABLE_RECIPIENT,
    };
    List<PatientHour> hourList;

    @Override
    public void setList(List newModels) {
        hourList = newModels;
    }

    @Override
    public Object getRowItem(int row) {
        return hourList.get(row);
    }

    @Override
    public int getRowCount() {
        return hourList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        PatientHour hour = hourList.get(rowIndex);
        return switch (columnIndex) {
            case 0 -> hour.getDate().toLocalDate();
            case 1 -> PatientHour.PATIENT_HOURS_TYPES_TO_STRING.get(hour.getType());
            case 2 -> 1;
            case 3 -> hour.getRecipient();
            default -> null;
        };
    }
}
