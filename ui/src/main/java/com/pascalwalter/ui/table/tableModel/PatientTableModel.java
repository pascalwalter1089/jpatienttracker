package com.pascalwalter.ui.table.tableModel;

import com.pascalwalter.model.models.Patient;
import com.pascalwalter.model.models.SupervisionHour;
import com.pascalwalter.service.PatientHourCountService;
import com.pascalwalter.utils.StringConstants;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class PatientTableModel extends UpdateableTableModel {
    private final String[] columnNames = {
            StringConstants.PATIENT_TABLE_MODEL_HASH,
            StringConstants.PATIENT_TABLE_MODE_THERAPIESTUNDEN,
            StringConstants.PATIENT_TABLE_MODEL_SUPERVISIONSSTUNDEN,
            StringConstants.PATIENT_TABLE_MODEL_STATUS
    };
    List<Patient> patientList;

    public PatientTableModel() {
        this.patientList = new ArrayList<>();
    }

    public void setPatientList(List<Patient> patientList) {
        this.patientList = patientList;
    }

    public Color getBackgroundForPatient(int row) {
        if (this.patientList.get(row).getStatus() == Patient.PATIENT_STATE_BEENDET) {
            return Color.ORANGE;
        }
        if (this.patientList.get(row).getStatus() == Patient.PATIENT_STATE_PAUSIERT) {
            return Color.YELLOW;
        }

        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        return Patient.class;
    }

    @Override
    public int getRowCount() {
        return this.patientList.size();
    }

    @Override
    public int getColumnCount() {
        return this.columnNames.length;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Patient patient = patientList.get(rowIndex);
        double supervisionHours = patient.getSupervisionHours().stream().toList().stream().mapToDouble(SupervisionHour::getDuration).sum();

        return switch (columnIndex) {
            case 0 -> patient.getHash();
            case 1 -> PatientHourCountService.calculatePatientHourCount(patient);
            case 2 -> supervisionHours;
            case 3 -> patient.getStatusAsString();
            default -> null;
        };
    }

    @Override
    public void setList(List newModels) {
        this.patientList =  newModels;
    }

    @Override
    public Object getRowItem(int row) {
        return this.patientList.get(row);
    }
}
