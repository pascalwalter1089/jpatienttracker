package com.pascalwalter.ui.table;

import com.pascalwalter.ui.table.tableModel.UpdateableTableModel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.util.List;

public class TablePanel extends JPanel {
    protected UpdateableTableModel tableModel;
    protected JTable table;

    public TablePanel(UpdateableTableModel tableModel) {
        super();
        initializeVariables(tableModel);
        initializeLayout();
        initializeHeaderAlignment();
        initializeTableAlignment();
    }

    private void initializeLayout() {
        setLayout(new BorderLayout());
        setBorder(new EmptyBorder(10, 30, 10, 30));
        add(new JScrollPane(this.table), BorderLayout.CENTER);
    }

    private void initializeVariables(UpdateableTableModel tableModel) {
        this.tableModel = tableModel;
        this.table = new JTable(this.tableModel);
    }

    private void initializeTableAlignment() {
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
        renderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);

        for(int i = 0; i < this.tableModel.getColumnCount(); i++) {
            table.getColumnModel().getColumn(i).setCellRenderer(renderer);
        }
    }

    private void initializeHeaderAlignment() {
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
        renderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
        table.getTableHeader().setDefaultRenderer(renderer);
    }

    public void setTableModel(List newModels) {
        this.tableModel.setList(newModels);
    }

    public void update() {
        this.tableModel.update();
    }

    public void setDefaultRenderer(Class stringClass, TableCellRenderer renderer) {
        this.table.setDefaultRenderer(stringClass, renderer);
    }

    public TableColumnModel getColumnModel() {
        return table.getColumnModel();
    }

    public JTable getTable()
    {
        return this.table;
    }
}
