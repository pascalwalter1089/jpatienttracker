package com.pascalwalter.ui.table.cellRenderer;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

public class MultiLineCellRenderer extends JLabel implements TableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        //make multi line where the cell value is String[]
        if (value instanceof String[] valueCasted) {
            StringBuilder labelString  = new StringBuilder("<html>" + valueCasted[0]);
            for (int i = 1; i < valueCasted.length; i++) {
                labelString.append("<br>").append(valueCasted[i]);
            }

            this.setText(labelString + "</html>");
        }

        setOpaque(true);
        //cell backgroud color when selected
        if (isSelected) {
            setBackground(UIManager.getColor("Table.selectionBackground"));
            setForeground(Color.white);
        } else {
            setBackground(UIManager.getColor("Table.background"));
            setForeground(UIManager.getColor("Table.foreground"));
        }

        return this;
    }
}
