package com.pascalwalter.ui.table;

import com.pascalwalter.model.models.Patient;
import com.pascalwalter.ui.callbacks.PatientDialogCallbackInterface;
import com.pascalwalter.ui.dialog.PatientDetails;
import com.pascalwalter.ui.table.cellRenderer.PatientTableCellRenderer;
import com.pascalwalter.ui.table.tableModel.UpdateableTableModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PatientTable extends TablePanel {

    private PatientDialogCallbackInterface parent = null;

    public PatientTable(UpdateableTableModel tableModel, PatientDialogCallbackInterface parent) {
        super(tableModel);
        initializeActionListener();
        this.parent = parent;
        for (int i = 0; i < this.table.getColumnCount(); i++) {
            this.table.getColumnModel().getColumn(i).setCellRenderer(new PatientTableCellRenderer());
        }
    }

    private void initializeActionListener() {
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                Point point = mouseEvent.getPoint();
                int row = PatientTable.this.table.rowAtPoint(point);
                if (mouseEvent.getClickCount() == 2 && row != -1) {
                    PatientDetails details = new PatientDetails(PatientTable.this, (Patient) PatientTable.this.tableModel.getRowItem(row));
                    details.setUpdateActionListener(parent);
                    details.setVisible(true);
                }
            }
        });
    }
}
