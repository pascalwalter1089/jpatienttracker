package com.pascalwalter.ui.table.tableModel;

import com.pascalwalter.model.models.SupervisionHour;
import com.pascalwalter.model.models.Supervisor;
import com.pascalwalter.utils.StringConstants;

import java.util.List;

public class SupervisorTableModel extends UpdateableTableModel {

    private final String[] columnNames = {"Name", "Art", "Anzahl Stunden"};
    private List<Supervisor> supervisorList;

    public SupervisorTableModel(List<Supervisor> supervisorList) {
        this.supervisorList = supervisorList;
    }
    @Override
    public void setList(List newModels) {
        this.supervisorList = newModels;
    }

    @Override
    public Object getRowItem(int row) {
        return supervisorList.get(row);
    }

    @Override
    public int getRowCount() {
        return this.supervisorList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Supervisor item = this.supervisorList.get(rowIndex);
        return switch (columnIndex) {
            case 0 -> item.getName();
            case 1 -> getSupervisionHourTypes(item);
            case 2 -> item.getHours().stream().mapToDouble(SupervisionHour::getDuration).sum();
            default -> null;
        };
    }

    private String getSupervisionHourTypes(Supervisor item) {
        boolean isSingle = false;
        boolean isGroup = false;
        for (SupervisionHour h : item.getHours()) {
            if (h.getType() == 0) {
                isSingle = true;
            }
            if (h.getType() == 1) {
                isGroup = true;
            }
        }

        if (isSingle && isGroup) {
            return StringConstants.SUPERVISION_HOUR_EINZEL + ", " + StringConstants.SUPERVISION_HOUR_GROUP;
        }

        if (!isGroup && isSingle) {
            return StringConstants.SUPERVISION_HOUR_EINZEL;
        }

        if (isGroup && !isSingle) {
            return StringConstants.SUPERVISION_HOUR_GROUP;
        }

        return "";
    }
}