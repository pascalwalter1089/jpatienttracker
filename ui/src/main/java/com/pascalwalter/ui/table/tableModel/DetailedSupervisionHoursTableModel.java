package com.pascalwalter.ui.table.tableModel;

import com.pascalwalter.model.models.Patient;
import com.pascalwalter.model.models.SupervisionHour;
import com.pascalwalter.utils.StringConstants;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class DetailedSupervisionHoursTableModel extends UpdateableTableModel {

    private final String[] columnNames = {
            StringConstants.PATIENT_DETAIL_TABLE_DATE,
            StringConstants.PATIENT_DETAIL_TABLE_TYPE,
            StringConstants.PATIENT_DETAIL_TABLE_DURATION,
            StringConstants.PATIENT_DETAIL_TABLE_PATIENT,
    };
    private List<SupervisionHour> hourList;

    public DetailedSupervisionHoursTableModel(List<SupervisionHour> models) {
        hourList = models;
    }

    @Override
    public void setList(List newModels) {
        this.hourList = newModels;
    }

    @Override
    public Object getRowItem(int row) {
        return this.hourList.get(row);
    }

    @Override
    public int getRowCount() {
        return this.hourList.size();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public int getColumnCount() {
        return this.columnNames.length;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return getValueAt(0, columnIndex).getClass();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        SupervisionHour hour = (SupervisionHour) getRowItem(rowIndex);
        return switch (columnIndex) {
            case 0 -> hour.getDate().toLocalDate();
            case 1 -> SupervisionHour.SUPERVISION_HOURS_TYPES_TO_STRING.get(hour.getType());
            case 2 -> hour.getDuration();
            case 3 -> {
                String[] hashes = new String[hour.getPatients().size()];
                String hashString = "";
                int count = 0;
                for (Patient p: hour.getPatients()) {
                    hashes[count] = p.getHash();
                    hashString += p.getHash() + "<br>";
                    count++;
                }

                yield hashes;
            }
            default -> null;
        };
    }
}
