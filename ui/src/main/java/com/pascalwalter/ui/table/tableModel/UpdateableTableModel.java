package com.pascalwalter.ui.table.tableModel;

import javax.swing.table.AbstractTableModel;
import java.util.*;

public abstract class UpdateableTableModel extends AbstractTableModel {
    public void update() {
        fireTableDataChanged();
    }

    public abstract void setList(List newModels);
    public abstract Object getRowItem(int row);
}
