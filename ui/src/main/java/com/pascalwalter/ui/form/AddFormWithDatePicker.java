package com.pascalwalter.ui.form;

import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;
import javafx.util.converter.LocalDateStringConverter;

import javax.swing.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public abstract class AddFormWithDatePicker extends JDialog {
    private static final String pattern = "dd-MMM-yy";
    protected DatePicker inPicker;
    protected JFXPanel datePickerPanel;

    protected Scene createScene() {
        inPicker = new DatePicker();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        StringConverter<LocalDate> converter =
                new LocalDateStringConverter(formatter, null);
        inPicker.setConverter(converter);
        inPicker.setDayCellFactory(getCustomDateCellFactory());
        inPicker.setValue(LocalDate.now());
        VBox vbox = new VBox(20);
        vbox.setPadding(new javafx.geometry.Insets(5, 5, 5, 5));
        vbox.getChildren().addAll(inPicker);

        return new Scene(vbox);
    }

    private Callback<DatePicker, DateCell> getCustomDateCellFactory() {
        Callback<DatePicker, DateCell> dayCellFactory =
                new Callback<>() {

                    @Override
                    public DateCell call(DatePicker datePicker) {

                        return new DateCell() {
                            @Override
                            public void updateItem(LocalDate select, boolean empty) {

                                super.updateItem(select, empty);

                                // Date cannot be after today.
                                // This applies for all the date pickers.
                                if (select.isAfter(LocalDate.now())) {
                                    setDisable(true);
                                }
                            }
                        };
                    }
                };

        return dayCellFactory;
    }
}
