package com.pascalwalter.ui.form;

import com.pascalwalter.model.models.Patient;
import com.pascalwalter.service.MainFrameService;
import com.pascalwalter.service.RemovePatientService;
import com.pascalwalter.ui.callbacks.DataUpdatedCallbackInterface;
import com.pascalwalter.utils.ButtonUtils;
import com.pascalwalter.utils.NumberConstants;
import com.pascalwalter.utils.StringConstants;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RemovePatientForm extends JDialog implements ActionListener {
    private DataUpdatedCallbackInterface callback;
    private JLabel patientSelectorLabel;
    private JComboBox<Patient> patientSelector;
    private MainFrameService mainFrameService;
    private JButton removeButton;
    private JButton cancelButton;
    private RemovePatientService removePatientService;

    public RemovePatientForm(JFrame parentFrame, DataUpdatedCallbackInterface callback) {
        initializeVariables();
        buildDialog(parentFrame);
        initActionListener();
        this.callback = callback;
    }

    private void initActionListener() {
        removeButton.addActionListener(this);
        cancelButton.addActionListener(this);
    }

    private void initializeVariables() {
        mainFrameService = new MainFrameService();
        patientSelectorLabel = new JLabel(StringConstants.PATIENT_ROVAL_PATIENT_SELECTOR_LABEL);
        patientSelector = new JComboBox<>();
        mainFrameService.listAllPatients().forEach(patientSelector::addItem);
        removeButton = new JButton(StringConstants.REMOVE);
        cancelButton = new JButton(StringConstants.FORM_CANCEL_BUTTON);
        removePatientService = new RemovePatientService();
    }

    private void buildDialog(JFrame parentFrame) {
        setLayout(new BorderLayout());
        setLocationRelativeTo(parentFrame);
        setSize(NumberConstants.LOGIN_FORM_WIDTH, NumberConstants.LOGIN_FORM_HEIGHT);

        Border spaceBorder = BorderFactory.createEmptyBorder(15,15,15,15);
        Border titleBorder = BorderFactory.createTitledBorder(StringConstants.REMOVE_PATIENT_TITLE);

        JPanel selectorPanel = new JPanel();
        JPanel buttonPanel = new JPanel();

        selectorPanel.setBorder(BorderFactory.createCompoundBorder(spaceBorder, titleBorder));
        selectorPanel.setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();

        Insets rightPadding = new Insets(0, 0, 0, 15);
        Insets noPadding = new Insets(0, 0, 0, 0);

        gc.gridy = 0;

        gc.weightx = 1;
        gc.weighty = 1;
        gc.fill = GridBagConstraints.NONE;

        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        selectorPanel.add(patientSelectorLabel, gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        selectorPanel.add(patientSelector, gc);

        Dimension newSize = ButtonUtils.getNewDimensionForButtons(removeButton, cancelButton);

        removeButton.setPreferredSize(newSize);
        cancelButton.setPreferredSize(newSize);

        buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        buttonPanel.add(cancelButton);
        buttonPanel.add(removeButton);

        add(selectorPanel, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(this.removeButton)) {
            Patient patient = (Patient) patientSelector.getSelectedItem();
            int answer = JOptionPane.showConfirmDialog(
                    this,
                    String.format(
                            StringConstants.REMOVE_PATIENT_CONFIRMATION_FORMAT,
                            patient.getHash()
                    ),
                    StringConstants.REMOVE_PATIENT_TITLE,
                    JOptionPane.OK_CANCEL_OPTION
            );

            if (answer == JOptionPane.OK_OPTION) {
                removePatientService.removePatient(patient);
            }
        }

        patientSelector.setSelectedIndex(-1);
        callback.dataUpdated();
        setVisible(false);
    }

    public void update() {
        patientSelector.removeAllItems();
        mainFrameService.listAllPatients().forEach(patientSelector::addItem);
    }
}
