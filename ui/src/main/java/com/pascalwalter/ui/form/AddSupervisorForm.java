package com.pascalwalter.ui.form;

import com.pascalwalter.model.models.Supervisor;
import com.pascalwalter.service.SupervisorFormService;
import com.pascalwalter.ui.MainFrame;
import com.pascalwalter.ui.callbacks.AddSupervisorFormCallbackInterface;
import com.pascalwalter.utils.NumberConstants;
import com.pascalwalter.utils.StringConstants;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddSupervisorForm extends JDialog implements ActionListener {
    private SupervisorFormService addSupervisorFormService;
    private JLabel nameLabel;
    private JTextField nameField;
    private JButton addSupervisorButton;
    private JButton cancelSupervisorButton;
    private AddSupervisorFormCallbackInterface callback;

    public AddSupervisorForm(JFrame parent) {
        initializeVariables();
        buildFrame(parent);
    }

    private void buildFrame(JFrame parent) {
        setTitle(StringConstants.ADD_SUPERVISOR_TITLE);
        setLayout(new BorderLayout());
        setLocationRelativeTo(parent);

        setSize(NumberConstants.LOGIN_FORM_WIDTH, NumberConstants.LOGIN_FORM_HEIGHT);
        JPanel supervisorPanel = new JPanel();
        JPanel buttonsPanel = new JPanel();

        Border spaceBorder = BorderFactory.createEmptyBorder(15,15,15,15);
        Border titleBorder = BorderFactory.createTitledBorder(StringConstants.ADD_SUPERVISOR_TITLE);

        supervisorPanel.setBorder(BorderFactory.createCompoundBorder(spaceBorder, titleBorder));
        supervisorPanel.setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();

        Insets rightPadding = new Insets(0, 0, 0, 15);
        Insets noPadding = new Insets(0, 0, 0, 0);

        gc.gridy = 0;

        gc.weightx = 1;
        gc.weighty = 1;
        gc.fill = GridBagConstraints.NONE;

        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        supervisorPanel.add(nameLabel, gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        supervisorPanel.add(nameField, gc);

        //Buttons pane
        buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        buttonsPanel.add(cancelSupervisorButton);
        buttonsPanel.add(addSupervisorButton);
        add(supervisorPanel, BorderLayout.CENTER);
        add(buttonsPanel, BorderLayout.SOUTH);
    }

    private void initializeVariables() {
        addSupervisorFormService = new SupervisorFormService();
        nameLabel = new JLabel(StringConstants.ADD_SUPERVISOR_FORM_NAME_LABEL);
        nameField = new JTextField(NumberConstants.LOGIN_FORM_TEXT_FIELD_COLUMN);
        addSupervisorButton = new JButton(StringConstants.FORM_ADD_BUTTON);
        addSupervisorButton.addActionListener(this);
        cancelSupervisorButton = new JButton(StringConstants.FORM_CANCEL_BUTTON);
        cancelSupervisorButton.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(addSupervisorButton)) {
            addSupervisorFormService.addSupervisor(new Supervisor(0, nameField.getText()));
            if (callback != null) {
                callback.supervisorAdded();
            }
        }

        nameField.setText("");
        setVisible(false);
    }

    public void setCallback(AddSupervisorFormCallbackInterface callback) {
        this.callback = callback;
    }
}
