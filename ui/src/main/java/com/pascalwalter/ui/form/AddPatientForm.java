package com.pascalwalter.ui.form;

import com.pascalwalter.model.models.Patient;
import com.pascalwalter.service.AddPatientFormService;
import com.pascalwalter.ui.callbacks.AddPatientCallbackInterface;
import com.pascalwalter.utils.ButtonUtils;
import com.pascalwalter.utils.NumberConstants;
import com.pascalwalter.utils.StringConstants;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddPatientForm extends JDialog implements ActionListener {
    private JLabel patientHash;
    private JTextField patientHashField;
    private AddPatientFormService addPatientService;
    private JButton addButton;
    private JButton cancelButton;
    private AddPatientCallbackInterface addPatientCallback = null;

    public AddPatientForm(JFrame parentFrame) {
        super(parentFrame, StringConstants.ADD_PATIENT_TITLE, true);
        initializeVariables();
        buildDialog(parentFrame);
    }

    private void buildDialog(JFrame parentFrame) {
        setLayout(new BorderLayout());
        setLocationRelativeTo(parentFrame);
        setSize(NumberConstants.LOGIN_FORM_WIDTH, NumberConstants.LOGIN_FORM_HEIGHT);

        JPanel patientPanel = new JPanel();
        JPanel buttonPanel = new JPanel();

        Border spaceBorder = BorderFactory.createEmptyBorder(15,15,15,15);
        Border titleBorder = BorderFactory.createTitledBorder(StringConstants.ADD_PATIENT_TITLE);

        patientPanel.setBorder(BorderFactory.createCompoundBorder(spaceBorder, titleBorder));
        patientPanel.setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();

        Insets rightPadding = new Insets(0, 0, 0, 15);
        Insets noPadding = new Insets(0, 0, 0, 0);

        gc.gridy = 0;

        gc.weightx = 1;
        gc.weighty = 1;
        gc.fill = GridBagConstraints.NONE;

        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        patientPanel.add(patientHash, gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        patientPanel.add(patientHashField, gc);

        Dimension newSize = ButtonUtils.getNewDimensionForButtons(addButton, cancelButton);

        addButton.setPreferredSize(newSize);
        cancelButton.setPreferredSize(newSize);

        buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        buttonPanel.add(cancelButton);
        buttonPanel.add(addButton);

        add(patientPanel, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);
    }

    private void initializeVariables() {
        patientHash = new JLabel(StringConstants.ADD_PATIENT_HASH_LABEL);
        patientHashField = new JTextField(NumberConstants.LOGIN_FORM_TEXT_FIELD_COLUMN);
        addPatientService = new AddPatientFormService();
        addButton = new JButton(StringConstants.FORM_ADD_BUTTON);
        addButton.addActionListener(this);
        cancelButton = new JButton(StringConstants.FORM_CANCEL_BUTTON);
        cancelButton.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(addButton)) {
            addPatientService.addPatient(new Patient(patientHashField.getText()));
            if (null != addPatientCallback) {
                addPatientCallback.patientAdded();
            }

        }
        this.setVisible(false);
        this.clearAllFields();
    }

    private void clearAllFields() {
        patientHashField.setText("");
    }

    public void setCallback(AddPatientCallbackInterface callback) {
        this.addPatientCallback = callback;
    }
}
