package com.pascalwalter.ui.form;

import com.pascalwalter.model.models.Patient;
import com.pascalwalter.model.models.PatientHour;
import com.pascalwalter.service.AddPatientHourFormService;
import com.pascalwalter.ui.callbacks.AddPatientHourCallbackInterface;
import com.pascalwalter.utils.NumberConstants;
import com.pascalwalter.utils.StringConstants;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.List;

import javafx.embed.swing.JFXPanel;
import javafx.application.Platform;

public class AddPatientHourForm extends AddFormWithDatePicker implements ActionListener {
    //TODO: Fix change of receiver not changing possible therapy hour types
    private AddPatientHourFormService addPatientHourFormService;
    private JLabel patientLabel;
    private JComboBox<Patient> patientSelector;
    private JLabel hourTypeSelectorLabel;

    private JComboBox<String> hourTypeSelector;
    private JButton addButton;
    private JButton cancelButton;
    private JLabel whoLabel;
    private JComboBox<PatientHour.PatientHourRecipient> whoSelector;
    private DefaultComboBoxModel<String> patientHourTypeModel;
    private DefaultComboBoxModel<String> bezugspersonHourTypeModel;
    private AddPatientHourCallbackInterface callback;
    private JLabel dateLabel;

    public AddPatientHourForm(JFrame parent) {
        initializeVariables();
        buildFrame(parent);
    }

    private void buildFrame(JFrame parent) {
        setLocationRelativeTo(parent);
        setLayout(new BorderLayout());

        setSize(NumberConstants.LOGIN_FORM_WIDTH, 330);
        JPanel addPatientHourPanel = new JPanel();
        JPanel buttonsPanel = new JPanel();

        Border spaceBorder = BorderFactory.createEmptyBorder(15,15,15,15);
        Border titleBorder = BorderFactory.createTitledBorder(StringConstants.ADD_PATIENT_HOUR_TITLE);

        addPatientHourPanel.setBorder(BorderFactory.createCompoundBorder(spaceBorder, titleBorder));
        addPatientHourPanel.setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();

        Insets rightPadding = new Insets(0, 0, 0, 15);
        Insets noPadding = new Insets(0, 0, 0, 0);

        gc.gridy = 0;

        gc.weightx = 1;
        gc.weighty = 1;
        gc.fill = GridBagConstraints.NONE;

        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        addPatientHourPanel.add(patientLabel, gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        addPatientHourPanel.add(patientSelector, gc);

        gc.gridy++;
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        addPatientHourPanel.add(whoLabel, gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        addPatientHourPanel.add(whoSelector, gc);

        gc.gridy++;
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        addPatientHourPanel.add(hourTypeSelectorLabel, gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        addPatientHourPanel.add(hourTypeSelector, gc);

        gc.gridy++;
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        addPatientHourPanel.add(dateLabel, gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;

        addPatientHourPanel.add(datePickerPanel, gc);

        //Buttons pane
        Dimension addButtonSize = addButton.getPreferredSize();
        Dimension cancelButtonSize = cancelButton.getPreferredSize();

        if (addButtonSize.getWidth() < cancelButtonSize.getWidth()) {
            addButtonSize = cancelButtonSize;
        } else {
            cancelButtonSize = addButtonSize;
        }

        addButton.setPreferredSize(addButtonSize);
        cancelButton.setPreferredSize(cancelButtonSize);
        buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        buttonsPanel.add(cancelButton);
        buttonsPanel.add(addButton);
        add(addPatientHourPanel, BorderLayout.CENTER);
        add(buttonsPanel, BorderLayout.SOUTH);
        Platform.runLater(() -> {
            datePickerPanel.setScene(createScene());
            //TODO: traversal from FX back to swing component!
        });
        revalidate();
        repaint();
    }

    private void initializeVariables() {
        addPatientHourFormService = new AddPatientHourFormService();
        patientLabel = new JLabel(StringConstants.ADD_PATIENT_HOUR_LABEL);
        patientSelector = new JComboBox<>();
        addPatientHourFormService.listLaufendePatients().forEach(p -> patientSelector.addItem(p));
        hourTypeSelectorLabel = new JLabel(StringConstants.ADD_PATIENT_HOUR_TYPE_LABEL);
        hourTypeSelector = new JComboBox<>();

        addButton = new JButton(StringConstants.FORM_ADD_BUTTON);
        addButton.addActionListener(this);
        cancelButton = new JButton(StringConstants.FORM_CANCEL_BUTTON);
        cancelButton.addActionListener(this);
        whoLabel = new JLabel(StringConstants.ADD_PATIENT_HOUR_WHO_LABEL);
        whoSelector = new JComboBox<>();
        whoSelector.addItem(PatientHour.PatientHourRecipient.PATIENT);
        whoSelector.addItem(PatientHour.PatientHourRecipient.BEZUGSPERSON);

        String[] patients = new String[PatientHour.PATIENT_HOUR_TYPES_PATIENT.length];
        for (int i = 0; i < patients.length; i++) {
            patients[i] = PatientHour.PATIENT_HOURS_TYPES_TO_STRING.get(PatientHour.PATIENT_HOUR_TYPES_PATIENT[i]);
        }
        patientHourTypeModel = new DefaultComboBoxModel<>(patients);

        hourTypeSelector.setModel(patientHourTypeModel);

        String[] bezugsperson = new String[PatientHour.PATIENT_HOUR_TYPES_BEZUGSPERSON.length];
        for (int i = 0; i < bezugsperson.length; i++) {
            bezugsperson[i] = PatientHour.PATIENT_HOURS_TYPES_TO_STRING.get(PatientHour.PATIENT_HOUR_TYPES_BEZUGSPERSON[i]);
        }

        bezugspersonHourTypeModel = new DefaultComboBoxModel<>(bezugsperson);
        whoSelector.addActionListener(e -> {
            JComboBox s = (JComboBox) e.getSource();
            if (s.getSelectedItem().toString().equals(StringConstants.PATIENT_HOUR_TYPE_PATIENT) ) {
                AddPatientHourForm.this.hourTypeSelector.setModel(patientHourTypeModel);
            }

            if (s.getSelectedItem().equals(StringConstants.PATIENT_HOUR_TYPE_BEZUGSPERSON)) {
                AddPatientHourForm.this.hourTypeSelector.setModel(bezugspersonHourTypeModel);
            }

            AddPatientHourForm.this.hourTypeSelector.setSelectedIndex(0);
            AddPatientHourForm.this.hourTypeSelector.repaint();
            AddPatientHourForm.this.hourTypeSelector.revalidate();
            AddPatientHourForm.this.repaint();
            AddPatientHourForm.this.revalidate();
        });

        datePickerPanel = new JFXPanel();
        dateLabel = new JLabel(StringConstants.ADD_PATIENT_HOUR_DATE_LABEL);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(addButton)) {
            Patient patient = (Patient) this.patientSelector.getSelectedItem();
            int type = PatientHour.PATIENT_HOURS_STRING_TO_TYPES.get((String) this.hourTypeSelector.getSelectedItem());
            PatientHour.PatientHourRecipient recip = (PatientHour.PatientHourRecipient) this.whoSelector.getSelectedItem();
            Date date = Date.valueOf(inPicker.getValue());
            addPatientHourFormService.addHoursForPatient(new PatientHour(
                    patient,
                    type,
                    recip,
                    date
            ));
            if (null != callback) {
                callback.patientHourAdded();
            }
        }
        this.patientSelector.setSelectedIndex(0);
        this.whoSelector.setSelectedIndex(0);
        this.hourTypeSelector.setSelectedIndex(0);
        this.setVisible(false);
    }

    public void update() {
        this.patientSelector.removeAllItems();
        List<Patient> patients = this.addPatientHourFormService.listLaufendePatients();
        patients.forEach(l -> this.patientSelector.addItem(l));
    }

    public void setCallback(AddPatientHourCallbackInterface callback) {
        this.callback = callback;
    }
}
