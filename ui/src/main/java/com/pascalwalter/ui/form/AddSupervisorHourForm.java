package com.pascalwalter.ui.form;

import com.pascalwalter.model.models.Patient;
import com.pascalwalter.model.models.SupervisionHour;
import com.pascalwalter.model.models.Supervisor;
import com.pascalwalter.service.AddSupervisionHourFormService;
import com.pascalwalter.ui.callbacks.AddSupervisionHourFormCallback;
import com.pascalwalter.utils.StringConstants;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;

public class AddSupervisorHourForm extends AddFormWithDatePicker implements ActionListener {

    private JButton addButton;
    private JButton cancelButton;
    private JLabel hoursLabel;
    private JLabel typeLabel;
    private JSpinner hoursSpinner;
    private JLabel dateLabel;
    private JScrollPane availablePatientScrollPane;
    private JScrollPane pickedPatientScrollPane;
    private JList<Patient> availablePatientList;
    private JList<Patient> pickedPatientList;
    private AddSupervisionHourFormService supervisionHourFormService;
    private JLabel supervisorLabel;
    private JComboBox<Supervisor> supervisorSelector;
    private JComboBox<String> typeSelector;
    private JButton addPatientButton;
    private JButton removePatientButton;
    private DefaultListModel<Patient> pickedPatientsModel;
    private DefaultListModel<Patient> patientDefaultListModel;
    private AddSupervisionHourFormCallback callback;

    public AddSupervisorHourForm(JFrame parent) {
        initializeVariables();
        initializeActionListener();
        buildDialog(parent);
    }

    private void initializeActionListener() {
        addButton.addActionListener(this);
        cancelButton.addActionListener(this);

        // Split pane

        availablePatientList.getSelectionModel().addListSelectionListener(l -> {
            pickedPatientList.clearSelection();
        });

        pickedPatientList.getSelectionModel().addListSelectionListener(l -> {
            availablePatientList.clearSelection();
        });


        addPatientButton.addActionListener(l -> {
            Patient p = AddSupervisorHourForm.this.availablePatientList.getSelectedValue();
            if (p != null) {
                AddSupervisorHourForm.this.patientDefaultListModel.removeElement(p);
                AddSupervisorHourForm.this.pickedPatientsModel.addElement(p);
                AddSupervisorHourForm.this.pickedPatientList.setModel(AddSupervisorHourForm.this.pickedPatientsModel);
            }
        });

        removePatientButton.addActionListener(l -> {
            Patient p = AddSupervisorHourForm.this.pickedPatientList.getSelectedValue();
            if (p != null) {
                AddSupervisorHourForm.this.pickedPatientsModel.removeElement(p);
                AddSupervisorHourForm.this.patientDefaultListModel.addElement(p);
                AddSupervisorHourForm.this.availablePatientList.setModel(AddSupervisorHourForm.this.patientDefaultListModel);
            }
        });
    }

    private void buildDialog(JFrame parent) {
        setLocationRelativeTo(parent);
        setLayout(new BorderLayout());
        setSize(500, 600);

        JPanel addSupervisionHourPanel = new JPanel();

        JPanel splitPanePanel = new JPanel();
        JPanel upperConfigsPanel = new JPanel();

        JPanel buttonsPanel = new JPanel(new FlowLayout());

        Border spaceBorder = BorderFactory.createEmptyBorder(15,15,15,15);
        Border titleBorder = BorderFactory.createTitledBorder(StringConstants.ADD_SUPERVISION_HOUR_TITLE);

        addSupervisionHourPanel.setBorder(BorderFactory.createCompoundBorder(spaceBorder, titleBorder));
        addSupervisionHourPanel.setLayout(new BorderLayout());

        // Upper input...
        buildUpperInput(upperConfigsPanel);

        // SplitPane...
        buildPatientSelector(splitPanePanel);

        // Buttons..
        buildButtonsPanel(buttonsPanel);

        addSupervisionHourPanel.add(upperConfigsPanel, BorderLayout.NORTH);
        addSupervisionHourPanel.add(splitPanePanel, BorderLayout.CENTER);

        add(addSupervisionHourPanel, BorderLayout.CENTER);
        add(buttonsPanel, BorderLayout.SOUTH);
        revalidate();
        repaint();
    }

    private void buildButtonsPanel(JPanel buttonsPanel) {
        Dimension addButtonSize = addButton.getPreferredSize();
        Dimension cancelButtonSize = cancelButton.getPreferredSize();

        if (addButtonSize.getWidth() < cancelButtonSize.getWidth()) {
            addButtonSize = cancelButtonSize;
        } else {
            cancelButtonSize = addButtonSize;
        }

        addButton.setPreferredSize(addButtonSize);
        cancelButton.setPreferredSize(cancelButtonSize);

        buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        buttonsPanel.add(cancelButton);
        buttonsPanel.add(addButton);
    }

    private void buildPatientSelector(JPanel splitPanePanel) {
        JPanel buttonSplitPanel = new JPanel();
        JPanel scroolPanel = new JPanel();
        buttonSplitPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

        buttonSplitPanel.add(removePatientButton);
        buttonSplitPanel.add(addPatientButton);

        scroolPanel.setLayout(new FlowLayout());
        scroolPanel.add(availablePatientScrollPane);
        scroolPanel.add(pickedPatientScrollPane);

        availablePatientScrollPane.setBorder(BorderFactory.createTitledBorder(new EmptyBorder(0,0,0,0), StringConstants.ADD_SUPERVISION_HOUR_PATIENT_LIST_TITLE));
        pickedPatientScrollPane.setBorder(BorderFactory.createTitledBorder(new EmptyBorder(0,0,0,0), StringConstants.ADD_SUPERVISION_HOUR_PICKED_PATIENT_LIST_TITLE));

        splitPanePanel.setLayout(new BorderLayout());
        splitPanePanel.add(scroolPanel, BorderLayout.CENTER);
        splitPanePanel.add(buttonSplitPanel, BorderLayout.SOUTH);
    }

    private void buildUpperInput(JPanel upperConfigsPanel) {
        upperConfigsPanel.setLayout(new GridBagLayout());
        upperConfigsPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 15, 0));
        GridBagConstraints gc = new GridBagConstraints();

        Insets rightPadding = new Insets(0, 0, 0, 15);
        Insets noPadding = new Insets(0, 0, 0, 0);

        gc.gridy = 0;

        gc.weightx = 1;
        gc.weighty = 1;
        gc.fill = GridBagConstraints.NONE;

        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        upperConfigsPanel.add(supervisorLabel, gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        upperConfigsPanel.add(supervisorSelector, gc);

        gc.gridy++;
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        upperConfigsPanel.add(typeLabel, gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        upperConfigsPanel.add(typeSelector, gc);

        gc.gridy++;
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        upperConfigsPanel.add(hoursLabel, gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        upperConfigsPanel.add(hoursSpinner, gc);

        gc.gridy++;
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        upperConfigsPanel.add(dateLabel, gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        upperConfigsPanel.add(datePickerPanel, gc);
        Platform.runLater(() -> {
            datePickerPanel.setScene(createScene());
            //TODO: traversal from FX back to swing component!
        });
    }

    private void initializeVariables() {
        supervisionHourFormService = new AddSupervisionHourFormService();

        supervisorLabel = new JLabel(StringConstants.ADD_SUPERVISION_HOUR_SUPERVISOR_LABEL);
        supervisorSelector = new JComboBox<>();

        supervisionHourFormService.listSupervisors().forEach(s -> supervisorSelector.addItem(s));

        addButton = new JButton(StringConstants.FORM_ADD_BUTTON);
        cancelButton = new JButton(StringConstants.FORM_CANCEL_BUTTON);
        hoursLabel = new JLabel(StringConstants.ADD_SUPERVISION_HOUR_DURATION_LABEL);
        typeLabel = new JLabel(StringConstants.ADD_SUPERVISOR_TYPE);
        dateLabel = new JLabel(StringConstants.ADD_PATIENT_HOUR_DATE_LABEL);
        hoursSpinner = new JSpinner(new SpinnerNumberModel(1.0, 0.5, 5.0, 0.5));
        typeSelector = new JComboBox<>(SupervisionHour.SUPERVISION_HOURS_TYPES_TO_STRING.values().toArray(new String[0]));
        datePickerPanel = new JFXPanel();

        patientDefaultListModel = new DefaultListModel<>();
        pickedPatientsModel = new DefaultListModel<>();

        supervisionHourFormService.listPatients().forEach(patientDefaultListModel::addElement);
        availablePatientList = new JList<>(patientDefaultListModel);
        availablePatientList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        availablePatientList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        availablePatientList.setVisibleRowCount(-1);
        pickedPatientList = new JList<>(pickedPatientsModel);
        pickedPatientList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        pickedPatientList.setLayoutOrientation(JList.HORIZONTAL_WRAP);

        availablePatientList.setMinimumSize(new Dimension(150, 300));
        pickedPatientList.setMinimumSize(availablePatientList.getMinimumSize());

        availablePatientScrollPane = new JScrollPane(availablePatientList, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        pickedPatientScrollPane = new JScrollPane(pickedPatientList, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        availablePatientScrollPane.setPreferredSize(availablePatientList.getMinimumSize());
        pickedPatientScrollPane.setPreferredSize(availablePatientList.getMinimumSize());

        removePatientButton = new JButton("<");
        addPatientButton= new JButton(">");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(addButton)) {
            SupervisionHour hour = new SupervisionHour();
            hour.setType(SupervisionHour.stringTypeToInt((String) this.typeSelector.getSelectedItem()));
            hour.setDate(Date.valueOf(inPicker.getValue()));
            hour.setSupervisor((Supervisor) this.supervisorSelector.getSelectedItem());

            Patient[] selectedPatients = new Patient[this.pickedPatientsModel.size()];

            for (int i = 0; i < selectedPatients.length; i++) {
                selectedPatients[i] = pickedPatientsModel.get(i);
                selectedPatients[i].addSupervisionHour(hour);
            }

            hour.setPatients(selectedPatients);
            hour.setDuration((Double) this.hoursSpinner.getValue());

            supervisionHourFormService.addSupervisionHour(hour);

            if (callback != null) {
                callback.supervisionHourAdded();
            }
        }

        setVisible(false);
        supervisorSelector.setSelectedIndex(0);
        pickedPatientsModel.removeAllElements();
        patientDefaultListModel.removeAllElements();
        supervisionHourFormService.listPatients().forEach(patientDefaultListModel::addElement);
        typeSelector.setSelectedIndex(0);
        hoursSpinner.setValue(1.0);
    }

    public void update() {
        patientDefaultListModel.removeAllElements();
        supervisionHourFormService.listPatients().forEach(patientDefaultListModel::addElement);
        availablePatientList.setModel(patientDefaultListModel);
        supervisorSelector.removeAllItems();
        supervisionHourFormService.listSupervisors().forEach(s -> supervisorSelector.addItem(s));
    }

    public void setCallback(AddSupervisionHourFormCallback callback) {
        this.callback = callback;
    }
}
