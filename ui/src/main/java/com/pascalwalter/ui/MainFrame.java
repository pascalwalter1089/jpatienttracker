package com.pascalwalter.ui;

import com.pascalwalter.model.models.User;
import com.pascalwalter.service.InitializeAppService;
import com.pascalwalter.service.MainFrameService;
import com.pascalwalter.service.folder.RootApplictaionDirectoryHandler;
import com.pascalwalter.ui.callbacks.*;
import com.pascalwalter.ui.dialog.AboutDialog;
import com.pascalwalter.ui.form.*;
import com.pascalwalter.ui.panel.CreateAccountPanel;
import com.pascalwalter.ui.panel.LoginPanel;
import com.pascalwalter.ui.panel.PatientStatisticsPanel;
import com.pascalwalter.ui.table.*;
import com.pascalwalter.ui.table.tableModel.PatientTableModel;
import com.pascalwalter.ui.table.tableModel.SupervisorTableModel;
import com.pascalwalter.utils.NumberConstants;
import com.pascalwalter.utils.StringConstants;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;

public class MainFrame extends JFrame implements LoginCallback, AddPatientCallbackInterface, AddSupervisorFormCallbackInterface, AddPatientHourCallbackInterface, AddSupervisionHourFormCallback, PatientDialogCallbackInterface, AccountCreatedCallback, DataUpdatedCallbackInterface
{
    private LoginPanel loginPanel;
    private boolean loggedIn;
    private MainFrameService mainFrameService;
    private User user;
    private StatusPanel statusPanel;
    private PatientTable patientTable;
    private AddPatientForm addPatientForm;

    private JTabbedPane hoursTabs;
    private SupervisionTable supervisionTable;
    private AddSupervisorForm addSupervisorForm;
    private AddPatientHourForm addPatientHoursForm;
    private AboutDialog aboutDialog;
    private AddSupervisorHourForm addSupervisorHourForm;
    private PatientStatisticsPanel generalPatientStatisticsPanel;
    private RemovePatientForm removePatientForm;

    public MainFrame() {
        super(StringConstants.APP_NAME);
        this.setSize(NumberConstants.APP_FRAME_WIDTH, NumberConstants.APP_FRAME_HEIGHT);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                shutdown();
            }
        });

        this.setLocationRelativeTo(null);
        this.setVisible(true);
        if (!RootApplictaionDirectoryHandler.doesApplicationDatabaseExist()) {
            add(new CreateAccountPanel(this));
            this.setVisible(true);
        } else if (!loggedIn) {
            loggedIn = false;
            loginPanel = new LoginPanel(this);
            add(loginPanel);
        }
    }

    private void initializeFrame() {
        initializeVariables();
        buildFrame();
        setJMenuBar(buildMenuBar());
    }

    private void buildContent() {
        this.getContentPane().removeAll();
        statusPanel.setLabelText(String.format("Logged in: %s", user.getUsername()));
        add(hoursTabs, BorderLayout.CENTER);
        add(generalPatientStatisticsPanel, BorderLayout.SOUTH);
    }

    private void initializeVariables() {
        mainFrameService = new MainFrameService();
        statusPanel = new StatusPanel();
        PatientTableModel ptm = new PatientTableModel();
        ptm.setPatientList(this.mainFrameService.listAllPatients());
        ptm.update();
        patientTable = new PatientTable(ptm, this);
        addPatientForm = new AddPatientForm(this);
        addPatientForm.setCallback(this);
        SupervisorTableModel stm = new SupervisorTableModel(this.mainFrameService.listAllSupervisors());
        supervisionTable = new SupervisionTable(stm);

        hoursTabs = new JTabbedPane(JTabbedPane.TOP,JTabbedPane.WRAP_TAB_LAYOUT);
        hoursTabs.addTab(StringConstants.HOURS_TABS_TITLE_PATIENT, patientTable);
        hoursTabs.addTab(StringConstants.HOURS_TABS_TITLE_SUPERVISION, supervisionTable);

        addSupervisorForm = new AddSupervisorForm(this);
        addSupervisorForm.setCallback(this);

        addPatientHoursForm = new AddPatientHourForm(this);
        addPatientHoursForm.setCallback(this);

        aboutDialog = new AboutDialog(this);
        addSupervisorHourForm = new AddSupervisorHourForm(this);
        addSupervisorHourForm.setCallback(this);

        generalPatientStatisticsPanel = new PatientStatisticsPanel();
        removePatientForm = new RemovePatientForm(this, this);
    }

    private void updateData() {
        SwingUtilities.invokeLater(() -> {
            this.patientTable.setTableModel(mainFrameService.listAllPatients());
            this.patientTable.update();

            this.supervisionTable.setTableModel(mainFrameService.listAllSupervisors());
            this.supervisionTable.update();

            this.addPatientHoursForm.update();
            this.addSupervisorHourForm.update();
            this.removePatientForm.update();

            // Do last after all other data was updated
            this.generalPatientStatisticsPanel.updateCharts();
        });
    }

    private JMenuBar buildMenuBar() {
        JMenuBar bar = new JMenuBar();
        JMenu fileMenu = new JMenu(StringConstants.MENU_FILE);

        JMenuItem openItem = new JMenuItem(StringConstants.MENU_FILE_OPEN);
        JMenuItem exitItem = new JMenuItem(StringConstants.MENU_FILE_EXIT);

        fileMenu.add(openItem);
        fileMenu.add(exitItem);

        // EDIT MENU
        JMenu editMenu = new JMenu(StringConstants.MENU_EDIT);

        JMenuItem addPatientItem = new JMenuItem(StringConstants.MENU_EDIT_ADD_PATIENT);
        JMenuItem removePatientItem = new JMenuItem(StringConstants.MENU_EDIT_REMOVE_PATIENT);
        JMenuItem addPatientHour = new JMenuItem(StringConstants.MENU_EDIT_ADD_PATIENT_HOUR);

        JMenuItem addSupervisorItem = new JMenuItem(StringConstants.MENU_EDIT_ADD_SUPERVISOR);
        JMenuItem removeSupervisorItem = new JMenuItem(StringConstants.MENU_EDIT_REMOVE_SUPERVISOR);
        JMenuItem addSupervisorHourItem = new JMenuItem(StringConstants.MENU_EDIT_ADD_SUPERVISOR_HOUR);

        editMenu.add(addPatientItem);
        editMenu.add(removePatientItem);
        editMenu.add(addPatientHour);
        editMenu.addSeparator();
        editMenu.add(addSupervisorItem);
        editMenu.add(removeSupervisorItem);
        editMenu.add(addSupervisorHourItem);

        exitItem.addActionListener(l -> MainFrame.this.shutdown());
        addPatientItem.addActionListener(l -> MainFrame.this.openAddPatientForm());
        removePatientItem.addActionListener(l -> MainFrame.this.openRemovePatientForm());
        addPatientHour.addActionListener(l -> {
            MainFrame.this.openAddPatientHourForm();
        });

        addSupervisorHourItem.addActionListener(l -> MainFrame.this.openAddSupervisionHourForm());

        addSupervisorItem.addActionListener(l -> MainFrame.this.openAddSupervisorForm());

        // Help
        JMenu helpMenu = new JMenu();
        helpMenu.setText(StringConstants.MENU_HELP);
        JMenuItem about = new JMenuItem();
        about.setText(StringConstants.MENU_HELP_ABOUT);

        about.addActionListener(l -> MainFrame.this.openAboutForm());

        helpMenu.add(about);

        bar.add(fileMenu);
        bar.add(editMenu);
        bar.add(helpMenu);

        return bar;
    }

    private void openRemovePatientForm() {
        removePatientForm.setVisible(true);
    }

    private void openAddSupervisionHourForm() {
        addSupervisorHourForm.setVisible(true);
    }

    private void openAboutForm() {
        this.aboutDialog.setVisible(true);
    }

    private void openAddPatientHourForm() {
        this.addPatientHoursForm.update();
        this.addPatientHoursForm.setVisible(true);
    }

    private void openAddSupervisorForm() {
        this.addSupervisorForm.setVisible(true);
    }

    private void openAddPatientForm() {
        this.addPatientForm.setVisible(true);
    }

    private void buildFrame() {
        this.setVisible(true);
        setLayout(new BorderLayout());
        add(statusPanel, BorderLayout.SOUTH);
    }

    private void shutdown() {
        int action = JOptionPane.showConfirmDialog(
                MainFrame.this,
                StringConstants.EXIT_CONFIRM_TEXT,
                StringConstants.EXIT_CONFIRM_TITLE,
                JOptionPane.OK_CANCEL_OPTION
        );

        if (action == JOptionPane.OK_OPTION) {
            System.gc();
            System.exit(0);
        }
    }

    @Override
    public void validLogin(User user) {
        this.user = user;
        SwingUtilities.invokeLater(() -> {
            HashMap<String, String> settings = new HashMap<>();
            settings.put(InitializeAppService.HIBERNATE_CONNECTION_PASSWORD, user.getPassword());
            settings.put(InitializeAppService.HIBERNATE_CONNECTION_USER, user.getUsername());
            try {
                InitializeAppService.initializeDatabase(settings);
                initializeFrame();
                buildContent();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(
                        MainFrame.this,
                        StringConstants.MESSAGE_LOGIN_FAILED,
                        "Login fehlgeschlagen",
                        JOptionPane.ERROR_MESSAGE
                );
            }
        });
    }

    @Override
    public void patientAdded() {
        updateData();
    }

    @Override
    public void supervisorAdded() {
        updateData();
    }

    @Override
    public void patientHourAdded() {
        updateData();
    }

    @Override
    public void supervisionHourAdded() {
        updateData();
    }

    @Override
    public void update() {
        updateData();
    }

    @Override
    public void accountCreated(User user) {
        SwingUtilities.invokeLater(() -> {
            HashMap<String, String> settings = new HashMap<>();
            settings.put(InitializeAppService.HIBERNATE_CONNECTION_USER, user.getUsername());
            settings.put(InitializeAppService.HIBERNATE_CONNECTION_PASSWORD, user.getPassword());
            InitializeAppService.initializeDatabase(settings);

            this.loggedIn = true;
            setVisible(false);
            this.getContentPane().removeAll();
            repaint();
            revalidate();
            validLogin(user);
            setVisible(true);
            repaint();
            revalidate();
        });
    }

    @Override
    public void dataUpdated() {
        this.updateData();
    }
}
