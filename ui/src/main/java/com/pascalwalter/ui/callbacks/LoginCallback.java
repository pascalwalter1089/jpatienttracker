package com.pascalwalter.ui.callbacks;

import com.pascalwalter.model.models.User;

public interface LoginCallback {
    void validLogin(User user);
}
