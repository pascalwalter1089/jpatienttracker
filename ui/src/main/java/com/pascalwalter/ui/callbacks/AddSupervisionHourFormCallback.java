package com.pascalwalter.ui.callbacks;

public interface AddSupervisionHourFormCallback {
    void supervisionHourAdded();
}
