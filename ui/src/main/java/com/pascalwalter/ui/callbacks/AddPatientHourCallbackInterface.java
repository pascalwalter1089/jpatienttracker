package com.pascalwalter.ui.callbacks;

public interface AddPatientHourCallbackInterface {
    public void patientHourAdded();
}
