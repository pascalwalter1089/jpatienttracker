package com.pascalwalter.ui.callbacks;

public interface DataUpdatedCallbackInterface {
    void dataUpdated();
}
