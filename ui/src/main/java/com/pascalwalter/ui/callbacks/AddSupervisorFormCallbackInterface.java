package com.pascalwalter.ui.callbacks;

public interface AddSupervisorFormCallbackInterface {
    void supervisorAdded();
}
