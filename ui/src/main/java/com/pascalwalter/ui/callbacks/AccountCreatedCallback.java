package com.pascalwalter.ui.callbacks;

import com.pascalwalter.model.models.User;

public interface AccountCreatedCallback {
    void accountCreated(User user);
}
