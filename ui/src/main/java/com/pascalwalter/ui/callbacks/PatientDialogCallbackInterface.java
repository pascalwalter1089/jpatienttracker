package com.pascalwalter.ui.callbacks;

public interface PatientDialogCallbackInterface {
    void update();
}
