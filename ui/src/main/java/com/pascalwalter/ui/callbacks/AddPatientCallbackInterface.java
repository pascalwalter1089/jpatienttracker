package com.pascalwalter.ui.callbacks;

public interface AddPatientCallbackInterface {
    void patientAdded();
}
