package com.pascalwalter.ui.list.selectionmodel;

import javax.swing.*;

public class NonSelectableSelectionModel extends DefaultListSelectionModel {
    @Override
    public void setSelectionInterval(int index0, int index1) {
        super.setSelectionInterval(-1, -1);
    }
}
