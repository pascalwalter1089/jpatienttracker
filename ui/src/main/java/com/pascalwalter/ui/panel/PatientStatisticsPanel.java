package com.pascalwalter.ui.panel;

import com.pascalwalter.service.GeneralPatientStatisticsService;
import com.pascalwalter.utils.StringConstants;
import org.knowm.xchart.*;
import org.knowm.xchart.style.PieStyler;
import org.knowm.xchart.style.Styler;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class PatientStatisticsPanel extends JPanel {
    private PieChart patientHoursPieChart;
    private CategoryChart supervisionHoursBarChart;
    private GeneralPatientStatisticsService generalPatientStatisticsService;

    public PatientStatisticsPanel() {
        initializeVariables();
        buildPanel();
    }

    private void buildPanel() {
        setLayout(new FlowLayout());
        updateCharts();
    }

    private void initializeVariables() {
        patientHoursPieChart = new PieChartBuilder()
                .width(450)
                .height(400)
                .title(StringConstants.GENERAL_PATIENT_HOURS_PIE_CHART_TITLE)
                .build();

        supervisionHoursBarChart = new CategoryChartBuilder()
                .width(450)
                .height(400)
                .title(StringConstants.GENERAL_SUPERVISION_HOURS_BARCHART)
                .xAxisTitle(StringConstants.GENERAL_SUPERVISION_STUNDENART)
                .yAxisTitle(StringConstants.GENERAL_SUPERVISION_STUNDENANZAHL)
                .theme(Styler.ChartTheme.XChart)
                .build();

        generalPatientStatisticsService = new GeneralPatientStatisticsService();
    }

    public void updateCharts() {
        removeAll();
        generalPatientStatisticsService.initValues();

        patientHoursPieChart.removeSeries(StringConstants.SERIES_NAME_GELEISTETE_STUNDEN);
        patientHoursPieChart.removeSeries(StringConstants.SERIES_NAME_ZULEISTENDE_STUNDEN);

        patientHoursPieChart.getStyler().setLabelType(PieStyler.LabelType.Value);
        patientHoursPieChart.getStyler().setToolTipsEnabled(true);

        patientHoursPieChart.addSeries(StringConstants.SERIES_NAME_GELEISTETE_STUNDEN, generalPatientStatisticsService.getGeleisteteStunden());
        patientHoursPieChart.addSeries(StringConstants.SERIES_NAME_ZULEISTENDE_STUNDEN, generalPatientStatisticsService.getZuleistendeStunden());

        add(new XChartPanel<>(patientHoursPieChart), BorderLayout.WEST);

        supervisionHoursBarChart.getStyler().setToolTipsEnabled(true);
        supervisionHoursBarChart.removeSeries(StringConstants.GENERAL_SUPERVISION_EINZEL);
        supervisionHoursBarChart.removeSeries(StringConstants.GENERAL_SUPERVISION_ZULEISTENDE_EINZEL);
        supervisionHoursBarChart.removeSeries(StringConstants.GENERAL_SUPERVISION_GRUPPEN);
        supervisionHoursBarChart.removeSeries(StringConstants.GENERAL_SUPERVISION_ZULEISTENDE_GRUPPEN);

        supervisionHoursBarChart.addSeries(
                StringConstants.GENERAL_SUPERVISION_EINZEL,
                new ArrayList<>(List.of(StringConstants.GENERAL_SUPERVISION_EINZEL)),
                new ArrayList<Number>(List.of(generalPatientStatisticsService.getEinzelSupervisionHour()))
        );
        supervisionHoursBarChart.addSeries(
                StringConstants.GENERAL_SUPERVISION_ZULEISTENDE_EINZEL,
                new ArrayList<>(List.of(StringConstants.GENERAL_SUPERVISION_ZULEISTENDE_EINZEL)),
                new ArrayList<Number>(List.of(generalPatientStatisticsService.getZuleistendeEinzelSupervisionHours()))
        );
        supervisionHoursBarChart.addSeries(
                StringConstants.GENERAL_SUPERVISION_GRUPPEN,
                new ArrayList<>(List.of(StringConstants.GENERAL_SUPERVISION_GRUPPEN)),
                new ArrayList<Number>(List.of(generalPatientStatisticsService.getGruppenSupervisionHour()))
        );
        supervisionHoursBarChart.addSeries(
                StringConstants.GENERAL_SUPERVISION_ZULEISTENDE_GRUPPEN,
                new ArrayList<>(List.of(StringConstants.GENERAL_SUPERVISION_ZULEISTENDE_GRUPPEN)),
                new ArrayList<Number>(List.of(generalPatientStatisticsService.getZuleistendeGruppenSupervisionHour()))
        );
        supervisionHoursBarChart.getStyler().setYAxisMax(100.0);
        supervisionHoursBarChart.getStyler().setYAxisMin(.0);
        supervisionHoursBarChart.getStyler().setYAxisTicksVisible(true);
        supervisionHoursBarChart.getStyler().setAxisTickMarkLength(10);
        add(new XChartPanel<>(supervisionHoursBarChart), BorderLayout.EAST);

        revalidate();
    }
}
