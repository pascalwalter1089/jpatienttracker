package com.pascalwalter.ui.panel;

import com.pascalwalter.model.models.User;
import com.pascalwalter.ui.callbacks.LoginCallback;
import com.pascalwalter.utils.NumberConstants;
import com.pascalwalter.utils.StringConstants;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginPanel extends JPanel implements ActionListener {
    private JLabel nameLabel;
    private JLabel passwordLabel;
    private JButton loginButton;
    private JTextField nameField;
    private JPasswordField passwordField;
    private LoginCallback loginCallback;

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException |
                 UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        SwingUtilities.invokeLater(() -> {
            JFrame jf = new JFrame();
            jf.setSize(NumberConstants.APP_FRAME_WIDTH, NumberConstants.APP_FRAME_HEIGHT);
            jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            jf.add(new LoginPanel());
            jf.setVisible(true);
        });
    }

    public LoginPanel() {
        initializeVariables();
        buildDialog();
    }

    public LoginPanel(LoginCallback callback) {
        initializeVariables();
        buildDialog();
        setCallback(callback);
    }

    private void initializeVariables() {
        nameLabel = new JLabel(StringConstants.LOGIN_FORM_NAME_LABEL);
        passwordLabel = new JLabel(StringConstants.LOGIN_FORM_PASSWORD_LABEL);

        nameField = new JTextField(NumberConstants.LOGIN_FORM_TEXT_FIELD_COLUMN);
        passwordField = new JPasswordField(NumberConstants.LOGIN_FORM_TEXT_FIELD_COLUMN);

        loginButton = new JButton(StringConstants.LOGIN_FORM_LOGIN_BUTTON);
        loginButton.addActionListener(this);
    }

    private void buildDialog() {
        setLayout(new BorderLayout());
        setSize(NumberConstants.LOGIN_FORM_WIDTH, NumberConstants.LOGIN_FORM_HEIGHT);
        JPanel loginPanel = new JPanel();
        JPanel buttonsPanel = new JPanel();

        Border spaceBorder = BorderFactory.createEmptyBorder(15, 15, 15, 15);
        Border titleBorder = BorderFactory.createTitledBorder(StringConstants.LOGIN_FORM_TITLE);

        loginPanel.setBorder(BorderFactory.createCompoundBorder(spaceBorder, titleBorder));
        loginPanel.setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();

        Insets rightPadding = new Insets(0, 0, 0, 15);
        Insets noPadding = new Insets(0, 0, 0, 0);

        gc.gridy = 0;
        gc.weightx = 1;
        gc.weighty = 1;
        gc.fill = GridBagConstraints.NONE;

        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        loginPanel.add(nameLabel, gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        loginPanel.add(nameField, gc);

        gc.gridy++;
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        loginPanel.add(passwordLabel, gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        loginPanel.add(passwordField, gc);

        //Buttons pane
        buttonsPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        buttonsPanel.add(loginButton);

        add(loginPanel, BorderLayout.NORTH);
        add(buttonsPanel, BorderLayout.CENTER);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        StringBuilder sb = new StringBuilder();
        for (char a : passwordField.getPassword()) {
            sb.append(a);
        }
        User user = new User(1, nameField.getText(), sb.toString());
        clearFields();
        loginCallback.validLogin(user);
    }

    private void clearFields() {
        nameField.setText("");
        passwordField.setText("");
    }

    public void setCallback(LoginCallback loginCallback) {
        this.loginCallback = loginCallback;
    }
}
