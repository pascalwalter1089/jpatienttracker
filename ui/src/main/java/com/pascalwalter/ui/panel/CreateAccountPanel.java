package com.pascalwalter.ui.panel;

import com.pascalwalter.model.models.User;
import com.pascalwalter.service.InitializeAppService;
import com.pascalwalter.service.accountcreation.AccountCreationService;
import com.pascalwalter.ui.callbacks.AccountCreatedCallback;
import com.pascalwalter.ui.callbacks.LoginCallback;
import com.pascalwalter.utils.NumberConstants;
import com.pascalwalter.utils.StringConstants;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CreateAccountPanel extends JPanel implements ActionListener {
    private AccountCreatedCallback callback;
    private JButton createButton;
    private JTextField loginNameField;
    private JPasswordField passwordFieldOne;
    private JPasswordField passwordFieldTwo;
    private JLabel loginNameLabel;
    private JLabel passwordOneLabel;
    private JLabel passwordTwoLabel;
    private AccountCreationService accountCreationService;
    private JLabel errorMessageLabel;
    private JTextField informationText;

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException|InstantiationException|IllegalAccessException|UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        SwingUtilities.invokeLater(() -> {
            JFrame jf = new JFrame();
            jf.setSize(NumberConstants.APP_FRAME_WIDTH, NumberConstants.APP_FRAME_HEIGHT);
            jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            jf.add(new CreateAccountPanel());
            jf.setVisible(true);
        });
    }

    public CreateAccountPanel() {
        initializeVariables();
        buildFrame();
    }

    public CreateAccountPanel(AccountCreatedCallback callback) {
        initializeVariables();
        buildFrame();
        this.callback = callback;
    }

    private void buildFrame() {
        setLayout(new BorderLayout());

        JPanel combinedLowerPanel = new JPanel();
        combinedLowerPanel.setLayout(new BorderLayout());

        JPanel fieldPanel = new JPanel();
        fieldPanel.setLayout(new BorderLayout());
        Border spaceBorder = BorderFactory.createEmptyBorder(15,15,15,15);
        Border titleBorder = BorderFactory.createTitledBorder(StringConstants.CREATE_ACCOUNT_TITLE);

        fieldPanel.setBorder(BorderFactory.createCompoundBorder(spaceBorder, titleBorder));
        fieldPanel.setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();

        Insets rightPadding = new Insets(0, 0, 0, 15);
        Insets noPadding = new Insets(0, 0, 0, 0);

        gc.gridy = 0;
        gc.weightx = 1;
        gc.weighty = 1;
        gc.fill = GridBagConstraints.NONE;

        gc.gridy++;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        fieldPanel.add(errorMessageLabel, gc);

        gc.gridy++;
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        fieldPanel.add(loginNameLabel, gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        fieldPanel.add(loginNameField, gc);

        gc.gridy++;
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        fieldPanel.add(passwordOneLabel, gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        fieldPanel.add(passwordFieldOne, gc);

        gc.gridy++;
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        fieldPanel.add(passwordTwoLabel, gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        fieldPanel.add(passwordFieldTwo, gc);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(createButton);



        combinedLowerPanel.add(fieldPanel, BorderLayout.NORTH);
        combinedLowerPanel.add(buttonPanel, BorderLayout.CENTER);

        add(informationText, BorderLayout.CENTER);
        add(combinedLowerPanel, BorderLayout.SOUTH);
    }

    private void initializeVariables() {
        createButton = new JButton(StringConstants.BUTTON_ACCOUNT_ERSTELLEN);
        createButton.addActionListener(this);
        loginNameField = new JTextField(NumberConstants.LOGIN_FORM_TEXT_FIELD_COLUMN);
        passwordFieldOne = new JPasswordField(NumberConstants.LOGIN_FORM_TEXT_FIELD_COLUMN);
        passwordFieldTwo = new JPasswordField(NumberConstants.LOGIN_FORM_TEXT_FIELD_COLUMN);

        loginNameLabel = new JLabel(StringConstants.LABEL_USERNAME);
        loginNameField.requestFocus();
        passwordOneLabel = new JLabel(StringConstants.LABEL_PASSWORD);
        passwordTwoLabel = new JLabel(StringConstants.LABEL_PASSWORT_WIEDERHOLEN);
        errorMessageLabel = new JLabel(" ");

        accountCreationService = new AccountCreationService();

        informationText = new JTextField();
        informationText.setText(StringConstants.INFORMATION_TEXT);
        informationText.setEditable(false);
        informationText.setRequestFocusEnabled(false);
        informationText.setFocusable(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        char[] passwordArrayOne = passwordFieldOne.getPassword();
        char[] passwordArrayTwo = passwordFieldTwo.getPassword();
        try {
            errorMessageLabel.setText("");
            accountCreationService.validateUserName(loginNameField.getText());
            accountCreationService.validatePassword(passwordArrayOne, passwordArrayTwo);

            StringBuilder passwordOneStringBuilder = new StringBuilder();
            for (char s : passwordArrayOne) {
                passwordOneStringBuilder.append(s);
            }

            if (null != this.callback) {
                callback.accountCreated(new User(1, loginNameField.getText(), passwordOneStringBuilder.toString()));
            }
        } catch (Exception exception) {
            exception.printStackTrace();
            errorMessageLabel.setText(exception.getMessage());
            errorMessageLabel.setForeground(Color.red);

            revalidate();
            repaint();
        }
    }
}
