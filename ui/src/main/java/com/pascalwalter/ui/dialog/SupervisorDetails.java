package com.pascalwalter.ui.dialog;

import com.pascalwalter.model.models.Supervisor;
import com.pascalwalter.service.SupervisionDetailsService;
import com.pascalwalter.ui.table.DetailedHoursTable;
import com.pascalwalter.ui.table.cellRenderer.MultiLineCellRenderer;
import com.pascalwalter.ui.table.tableModel.DetailedSupervisionHoursTableModel;
import com.pascalwalter.utils.NumberConstants;
import com.pascalwalter.utils.StringConstants;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class SupervisorDetails  extends JDialog {
    private final Supervisor supervisor;
    private final JComponent parent;
    private DetailedSupervisionHoursTableModel tableModel;
    private DetailedHoursTable detailedHoursTable;
    private JLabel groupSupervisionHoursLabel;
    private JLabel soloSupervisionHoursLabel;
    private SupervisionDetailsService supervisorDetailsService;

    public SupervisorDetails(JComponent parent, Supervisor supervisor) {
        this.supervisor = supervisor;
        this.parent = parent;
        initializeVariables();
        buildFrame();
        revalidate();
    }

    private void buildFrame() {
        setTitle(this.supervisor.getName());
        setLocationRelativeTo(this.parent);
        setLayout(new BorderLayout());
        setSize(NumberConstants.DETAILS_DIALOG_WIDTH, NumberConstants.DETAILS_DIALOG_HEIGHT);

        JPanel details = new JPanel();
        details.setLayout(new GridLayout(2, 0, 5, 5));
        Border spaceBorder = BorderFactory.createEmptyBorder(15,15,15,15);
        Border titleBorder = BorderFactory.createTitledBorder(StringConstants.SUPERVISION_DETAILS_LABEL);
        details.setBorder(BorderFactory.createCompoundBorder(spaceBorder, titleBorder));

        JPanel numbers = new JPanel();
        numbers.setLayout(new GridLayout(2, 0, 15, 15));
        soloSupervisionHoursLabel.setText(String.format(StringConstants.SUPERVISION_HOUR_DETAILS_SOLO_LABEL, supervisorDetailsService.getCountOfSoloSupervisionHours(supervisor)));
        groupSupervisionHoursLabel.setText(String.format(StringConstants.SUPERVISION_HOUR_DETAILS_GROUP_LABEL, supervisorDetailsService.getCountOfGroupSupervisionHours(supervisor)));

        numbers.add(soloSupervisionHoursLabel);
        numbers.add(groupSupervisionHoursLabel);

        details.add(numbers);
        details.add(detailedHoursTable);
        detailedHoursTable.revalidate();
        this.add(details);
        this.setModal(true);
    }

    private void initializeVariables() {
        supervisorDetailsService = new SupervisionDetailsService();
        groupSupervisionHoursLabel = new JLabel();
        soloSupervisionHoursLabel = new JLabel();

        tableModel = new DetailedSupervisionHoursTableModel(supervisor.getHours().stream().toList());
        detailedHoursTable = new DetailedHoursTable(tableModel);
        detailedHoursTable.getColumnModel().getColumn(3).setCellRenderer(new MultiLineCellRenderer());
        for (int i = 0; i < tableModel.getRowCount(); i++) {
            String[] a = (String[]) tableModel.getValueAt(i, 3);
            detailedHoursTable.getTable().setRowHeight(i, a.length * 25);
        }
    }
}
