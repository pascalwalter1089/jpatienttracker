package com.pascalwalter.ui.dialog;

import com.pascalwalter.utils.NumberConstants;
import com.pascalwalter.utils.StringConstants;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class AboutDialog extends JDialog {

    private JTextPane aboutTextTextField;
    private JButton okButton;

    public AboutDialog(JFrame parent) {
        initializeVariables();
        buildDialog(parent);
    }

    private void buildDialog(JFrame parent) {
        setTitle(StringConstants.ABOUT_DIALOG_TITLE);
        setLocationRelativeTo(parent);
        setLayout(new BorderLayout());
        setSize(NumberConstants.LOGIN_FORM_WIDTH, NumberConstants.LOGIN_FORM_HEIGHT);

        Border spaceBorder = BorderFactory.createEmptyBorder(15,15,15,15);
        JPanel textPanel = new JPanel(new BorderLayout());
        textPanel.setBorder(spaceBorder);

        textPanel.add(aboutTextTextField, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel(new FlowLayout());
        buttonPanel.add(okButton);

        add(textPanel, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);
    }

    private void initializeVariables() {
        aboutTextTextField= new JTextPane();
        aboutTextTextField.setText(StringConstants.ABOUT_DIALOG_NOTICE);
        aboutTextTextField.setEditable(false);

        okButton = new JButton();
        okButton.setText(StringConstants.ABOUT_DIALOG_OK);
        okButton.addActionListener(l -> AboutDialog.this.setVisible(false));
    }

}
