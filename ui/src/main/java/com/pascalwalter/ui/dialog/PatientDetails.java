package com.pascalwalter.ui.dialog;

import com.pascalwalter.model.models.Patient;
import com.pascalwalter.service.PatientDetailsService;
import com.pascalwalter.service.UpdatePatientService;
import com.pascalwalter.ui.callbacks.PatientDialogCallbackInterface;
import com.pascalwalter.ui.list.selectionmodel.NonSelectableSelectionModel;
import com.pascalwalter.ui.table.DetailedHoursTable;
import com.pascalwalter.ui.table.tableModel.DetailedPatientHoursTableModel;
import com.pascalwalter.ui.table.PatientTable;
import com.pascalwalter.utils.ButtonUtils;
import com.pascalwalter.utils.NumberConstants;
import com.pascalwalter.utils.StringConstants;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PatientDetails extends JDialog implements ActionListener {
    private final Component parent;
    private Patient patient;
    private DetailedPatientHoursTableModel tableModel;
    private DetailedHoursTable detailedPatientHoursTable;
    private JButton setBeendetButton;
    private JButton setPausiertButton;
    private JButton setWiederaufnehmenButton;
    private PatientDialogCallbackInterface patientDialogCallback = null;
    private UpdatePatientService updatePatientService;
    private JPanel settingsPanel;

    public PatientDetails(PatientTable parent, Patient patient) {
        this.patient = patient;
        this.parent = parent;
        initializeVariables();
        initializeActionListener();
        buildFrame();
    }

    private void initializeActionListener() {
        this.setBeendetButton.addActionListener(this);
        this.setPausiertButton.addActionListener(this);
        this.setWiederaufnehmenButton.addActionListener(this);
    }

    private void initializeVariables() {
        tableModel = new DetailedPatientHoursTableModel(patient.getHours());
        detailedPatientHoursTable = new DetailedHoursTable(tableModel);

        setBeendetButton = new JButton("Beenden");
        setPausiertButton = new JButton("Pausieren");
        setWiederaufnehmenButton = new JButton("Wiederaufnehmen");

        updatePatientService = new UpdatePatientService();
        settingsPanel = new JPanel();
    }

    private void buildFrame() {
        setTitle(patient.getHash());
        setLocationRelativeTo(parent);
        setLayout(new BorderLayout());
        setSize(NumberConstants.DETAILS_DIALOG_WIDTH, NumberConstants.DETAILS_DIALOG_HEIGHT);

        JPanel details = new JPanel();
        Border spaceBorder = BorderFactory.createEmptyBorder(15,15,15,15);
        Border titleBorder = BorderFactory.createTitledBorder(StringConstants.PATIENT_DETAILS_DIALOG);
        details.setBorder(BorderFactory.createCompoundBorder(spaceBorder, titleBorder));

        details.setLayout(new GridLayout(2, 0, 5, 5));
        JPanel upperPanel = new JPanel();
        upperPanel.setLayout(new BorderLayout());
        upperPanel.add(buildSummaryPanel(), BorderLayout.WEST);
        upperPanel.add(buildSettingsPanel(), BorderLayout.EAST);
        details.add(upperPanel);
        details.add(detailedPatientHoursTable);

        this.add(details);
        this.setModal(true);
        this.revalidate();
    }

    private JPanel buildSettingsPanel() {
        settingsPanel.setLayout(new GridLayout(4,0));

        Dimension newButtonSize = ButtonUtils.getNewDimensionForButtons(setBeendetButton, setPausiertButton, setWiederaufnehmenButton);
        setPausiertButton.setPreferredSize(newButtonSize);
        setBeendetButton.setPreferredSize(newButtonSize);
        setWiederaufnehmenButton.setPreferredSize(newButtonSize);

        if (patient.getStatus() == Patient.PATIENT_STATE_LAUFEND) {
            this.settingsPanel.add(setPausiertButton);
            this.settingsPanel.add(setBeendetButton);
        } else {
            this.settingsPanel.add(setWiederaufnehmenButton);
        }

        return settingsPanel;
    }

    private JPanel buildSummaryPanel() {
        JPanel summaryPanel = new JPanel();
        summaryPanel.setLayout(new BorderLayout());
        JList<String> summaryList = new JList<>(new String[]{
                String.format("Therapiestunden Patient: %s", PatientDetailsService.calculateTotalTherapyHoursForPatient(patient)),
                String.format("Therapiestunden Bezugsperson: %s", PatientDetailsService.calculateTotalBezugspersonenHoursForPatient(patient)),
                String.format("Therapiestunden Gesamt: %s", PatientDetailsService.calculateTotalTherapyHours(patient)),
                String.format("Supervisionsstunden für Patient: %s", PatientDetailsService.calculateTotalSupervisionHoursForPatient(patient))
        });
        summaryList.setSelectionModel(new NonSelectableSelectionModel());
        summaryPanel.add(summaryList, BorderLayout.CENTER);
        return summaryPanel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(this.setBeendetButton) &&
                patientStateChangeConfirmation(StringConstants.CONFIRMATION_TEXT_END_TREATMENT, StringConstants.CONFIRMATION_TITLE_END_TREATMENT)) {
            this.patient.setStatus(Patient.PATIENT_STATE_BEENDET);
        } else if (e.getSource().equals(setPausiertButton) &&
                patientStateChangeConfirmation(StringConstants.CONFIRMATION_TEXT_PAUSE_TREATMENT, StringConstants.CONFIRMATION_TITLE_PAUSE_TREATMENT)) {
            this.patient.setStatus(Patient.PATIENT_STATE_PAUSIERT);
        } else if (e.getSource().equals(setWiederaufnehmenButton) &&
                patientStateChangeConfirmation(StringConstants.CONFIRMATION_TEXT_CONTINUE_TREATMENT, StringConstants.CONFIRMATION_TITLE_CONTINUE_TREATMENT)) {
            this.patient.setStatus(Patient.PATIENT_STATE_LAUFEND);
        } else {
            return;
        }

        this.patient = updatePatientService.updatePatient(this.patient);

        if (this.patientDialogCallback != null) {
            this.patientDialogCallback.update();
        }

        updateUI();
    }

    private boolean patientStateChangeConfirmation(String text, String title) {
        int action = JOptionPane.showConfirmDialog(
                PatientDetails.this,
                text,
                title,
                JOptionPane.OK_CANCEL_OPTION
        );

        return action == JOptionPane.OK_OPTION;
    }

    private void updateUI() {
        this.settingsPanel.removeAll();
        this.buildSettingsPanel();
        revalidate();
        repaint();
    }

    public void setUpdateActionListener(PatientDialogCallbackInterface parent) {
        this.patientDialogCallback = parent;
    }
}
