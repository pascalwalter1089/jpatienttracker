package com.pascalwalter.ui;

import com.pascalwalter.utils.StringConstants;

import javax.swing.*;
import java.awt.*;

public class StatusPanel extends JPanel {
    private JLabel label;
    public StatusPanel() {
        initializeVariables();
        initializeLayout();
    }

    private void initializeLayout() {
        setLayout(new FlowLayout());
        add(label);
    }

    private void initializeVariables() {
        label = new JLabel(StringConstants.APP_NAME);
    }

    public void setLabelText(String newText) {
        SwingUtilities.invokeLater(() -> {
            StatusPanel.this.label.setText(newText);
        });
    }
}
