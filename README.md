# JPatientTracker

## Disclaimer
Since the processing of data of patients is highly regulated and this software is not fulfilling all the requirements,
it is not allowed to use it to store and process real data. The author of the project creates and updates the software
only for training purposes!
