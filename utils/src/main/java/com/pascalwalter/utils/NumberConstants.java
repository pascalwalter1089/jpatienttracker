package com.pascalwalter.utils;

public class NumberConstants {
    public static final int LOGIN_FORM_WIDTH = 400;
    public static final int LOGIN_FORM_HEIGHT = 220;
    public static final int LOGIN_FORM_TEXT_FIELD_COLUMN = 15;
    public static int APP_FRAME_WIDTH = 1000;
    public static int APP_FRAME_HEIGHT = 800;
    public static int DETAILS_DIALOG_WIDTH = 750;
    public static int DETAILS_DIALOG_HEIGHT = 400;
    private NumberConstants() {}
}
