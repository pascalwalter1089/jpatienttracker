package com.pascalwalter.utils;

import javax.swing.*;
import java.awt.*;

import static java.lang.Double.max;

public class ButtonUtils {
    public static Dimension getNewDimensionForButtons(JButton... btns) {
        double maxWidthValue = btns[0].getPreferredSize().getWidth();
        double maxHeightValue = btns[0].getPreferredSize().getHeight();
        for (JButton button : btns) {
            maxWidthValue = max(maxWidthValue, button.getPreferredSize().getWidth());
            maxHeightValue = max(maxHeightValue, button.getPreferredSize().getHeight());
        }
        Dimension d = new Dimension();
        d.setSize(maxWidthValue, maxHeightValue);

        return d;
    }
}
