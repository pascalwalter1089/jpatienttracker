package com.pascalwalter.model;

import com.pascalwalter.model.models.*;

import java.util.List;

public interface DatabaseInterface {
    User getUser(String username, String password);
    User addUser(User user);

    Supervisor getSupervisor(int id);
    Supervisor addSupervisor(Supervisor supervisor);
    List<Supervisor> listSupervisors();

    Patient getPatient(int id);
    Patient addPatient(Patient patient);
    List<Patient> listPatients();

    PatientHour getPatientHour(int id);
    PatientHour addPatientHour(PatientHour patient);
    List<PatientHour> listPatientHours();
    List<PatientHour> listPatientHoursByPatient(int patientId);
    List<PatientHour> listPatientHoursByType(int patientHourType);

    SupervisionHour getSupervisionHour(int id);
    SupervisionHour addSupervisionHour(SupervisionHour supervisionHour);
    List<SupervisionHour> listSupervisionHour();
    List<SupervisionHour> listSupervisionHoursByPatient(Patient patient);
    List<SupervisionHour> listSupervisionHoursBySupervisor(int supervisorId);

    List<SupervisionHour> listSupervisionHoursByType(int supervisionHoursType);

    List<SupervisionHour> listSupervisionHoursBySupervisorAndType(Supervisor supervisor, int supervisionHoursType);
    List<Patient> listPatientsInStatus(int status);
    Patient updatePatient(Patient patient);

    boolean isConnected();

    void removePatient(Patient patient);
}
