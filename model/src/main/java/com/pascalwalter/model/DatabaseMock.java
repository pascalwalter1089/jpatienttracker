package com.pascalwalter.model;

import com.pascalwalter.model.models.*;

import java.util.ArrayList;
import java.util.List;

public class DatabaseMock implements DatabaseInterface {

    private ArrayList<Supervisor> supervisors;
    private ArrayList<Patient> patients;
    private ArrayList<PatientHour> patientHours;
    private ArrayList<SupervisionHour> supervisionHours;
    private ArrayList<User> users;

    public DatabaseMock() {
        patients = new ArrayList<Patient>();
        patients.add(new Patient( "PW101289"));
        patients.add(new Patient( "MW271292"));
        patients.add(new Patient( "LE031289"));

        supervisors = new ArrayList<Supervisor>();
        supervisors.add(new Supervisor(1, "Ryan Gosling"));
        supervisors.add(new Supervisor(2, "Emma Stone"));
    }
    @Override
    public User getUser(String username, String password) {
        return null;
    }

    @Override
    public User addUser(User user) {
        return null;
    }

    @Override
    public Supervisor getSupervisor(int id) {
        return null;
    }

    @Override
    public Supervisor addSupervisor(Supervisor supervisor) {
        supervisor.setId(supervisors.size());
        supervisors.add(supervisor);
        return supervisor;
    }

    @Override
    public List<Supervisor> listSupervisors() {
        return this.supervisors;
    }

    @Override
    public Patient getPatient(int id) {
        return null;
    }

    @Override
    public Patient addPatient(Patient patient) {
        patient.setId(patients.size());
        this.patients.add(patient);

        return patient;
    }

    @Override
    public List<Patient> listPatients() {
        return this.patients;
    }

    @Override
    public PatientHour getPatientHour(int id) {
        return null;
    }

    @Override
    public PatientHour addPatientHour(PatientHour patient) {
        return null;
    }

    @Override
    public List<PatientHour> listPatientHours() {
        return null;
    }

    @Override
    public List<PatientHour> listPatientHoursByPatient(int patientId) {
        return null;
    }

    @Override
    public List<PatientHour> listPatientHoursByType(int patientHourType) {
        return null;
    }

    @Override
    public SupervisionHour getSupervisionHour(int id) {
        return null;
    }

    @Override
    public SupervisionHour addSupervisionHour(SupervisionHour supervisionHour) {
        return null;
    }

    @Override
    public List<SupervisionHour> listSupervisionHour() {
        return null;
    }

    @Override
    public List<SupervisionHour> listSupervisionHoursByPatient(Patient patientId) {
        return null;
    }

    @Override
    public List<SupervisionHour> listSupervisionHoursBySupervisor(int supervisorId) {
        return null;
    }

    @Override
    public List<SupervisionHour> listSupervisionHoursByType(int supervisionHoursType) {
        return List.of();
    }

    @Override
    public List<SupervisionHour> listSupervisionHoursBySupervisorAndType(Supervisor supervisor, int supervisionHoursType) {
        return List.of();
    }

    @Override
    public List<Patient> listPatientsInStatus(int status) {
        return List.of();
    }

    @Override
    public Patient updatePatient(Patient patient) {
        return null;
    }

    @Override
    public boolean isConnected() {
        return false;
    }

    @Override
    public void removePatient(Patient patient) {

    }
}
