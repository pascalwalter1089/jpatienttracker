package com.pascalwalter.model.query;

import com.pascalwalter.model.DatabaseHandler;
import com.pascalwalter.model.DatabaseInterface;
import com.pascalwalter.model.models.Patient;

import java.util.List;

public class ListPatientsWithStatus {
    DatabaseInterface database = DatabaseHandler.INSTANCE.getDatabase();

    public List<Patient> listPatientsWithStatus(int status) {
        return database.listPatientsInStatus(status);
    }
}
