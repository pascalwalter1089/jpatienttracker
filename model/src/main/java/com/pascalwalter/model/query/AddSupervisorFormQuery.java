package com.pascalwalter.model.query;

import com.pascalwalter.model.DatabaseHandler;
import com.pascalwalter.model.DatabaseInterface;
import com.pascalwalter.model.models.Supervisor;

public class AddSupervisorFormQuery {
    DatabaseInterface database = DatabaseHandler.INSTANCE.getDatabase();

    public Supervisor addSupervisor(Supervisor supervisor) {
        return database.addSupervisor(supervisor);
    }
}
