package com.pascalwalter.model.query;

import com.pascalwalter.model.DatabaseHandler;
import com.pascalwalter.model.DatabaseInterface;
import com.pascalwalter.model.models.Patient;

public class RemovePatientQuery {
    DatabaseInterface database = DatabaseHandler.INSTANCE.getDatabase();

    public void removePatient(Patient patient) {
        database.removePatient(patient);
    }
}
