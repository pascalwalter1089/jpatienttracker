package com.pascalwalter.model.query;

import com.pascalwalter.model.DatabaseHandler;
import com.pascalwalter.model.DatabaseInterface;
import com.pascalwalter.model.models.Patient;
import com.pascalwalter.model.models.Supervisor;

import java.util.List;

public class MainFrameQueryImpl implements MainFrameQueryInterface {
    DatabaseInterface database = DatabaseHandler.INSTANCE.getDatabase();

    public List<Patient> listAllPatients() {
        return database.listPatients();
    }

    public List<Supervisor> listAllSupervisors() {
        return database.listSupervisors();
    }
}
