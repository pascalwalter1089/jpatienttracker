package com.pascalwalter.model.query;

import com.pascalwalter.model.DatabaseHandler;
import com.pascalwalter.model.DatabaseInterface;
import com.pascalwalter.model.models.Patient;
import com.pascalwalter.model.models.SupervisionHour;

import java.util.List;

public class PatientsHoursTableQuery {
    DatabaseInterface database = DatabaseHandler.INSTANCE.getDatabase();

    public List<SupervisionHour> listSupervisionHoursByPatient(Patient patient) {
        return database.listSupervisionHoursByPatient(patient);
    }
}
