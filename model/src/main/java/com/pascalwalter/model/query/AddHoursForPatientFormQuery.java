package com.pascalwalter.model.query;

import com.pascalwalter.model.DatabaseHandler;
import com.pascalwalter.model.DatabaseInterface;
import com.pascalwalter.model.models.PatientHour;

public class AddHoursForPatientFormQuery {
    private DatabaseInterface database = DatabaseHandler.INSTANCE.getDatabase();

    public PatientHour addPatientHour(PatientHour patientHour) {
        return database.addPatientHour(patientHour);
    }
}
