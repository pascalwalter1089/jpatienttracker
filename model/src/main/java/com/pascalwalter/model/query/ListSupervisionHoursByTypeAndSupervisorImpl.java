package com.pascalwalter.model.query;

import com.pascalwalter.model.DatabaseHandler;
import com.pascalwalter.model.DatabaseInterface;
import com.pascalwalter.model.models.SupervisionHour;
import com.pascalwalter.model.models.Supervisor;

import java.util.List;

public class ListSupervisionHoursByTypeAndSupervisorImpl implements ListSupervisionHoursByTypeAndSupervisorQueryInterface {
    DatabaseInterface database = DatabaseHandler.INSTANCE.getDatabase();

    @Override
    public List<SupervisionHour> listHoursByTypeAndSupervisor(Supervisor supervisor, int supervisionHoursType) {
        return database.listSupervisionHoursBySupervisorAndType(supervisor, supervisionHoursType);
    }
}
