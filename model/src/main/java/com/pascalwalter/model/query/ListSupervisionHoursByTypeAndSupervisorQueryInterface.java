package com.pascalwalter.model.query;

import com.pascalwalter.model.models.SupervisionHour;
import com.pascalwalter.model.models.Supervisor;

import java.util.List;

public interface ListSupervisionHoursByTypeAndSupervisorQueryInterface {
    List<SupervisionHour> listHoursByTypeAndSupervisor(Supervisor supervisor, int supervisionHoursType);
}
