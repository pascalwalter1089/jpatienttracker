package com.pascalwalter.model.query;

import com.pascalwalter.model.DatabaseHandler;
import com.pascalwalter.model.DatabaseInterface;
import com.pascalwalter.model.models.SupervisionHour;

import java.util.List;

public class ListSupervisionHoursByTypeImpl implements ListSupervisionHoursByTypeInterface {
    DatabaseInterface database = DatabaseHandler.INSTANCE.getDatabase();

    public List<SupervisionHour> listSupervisionHoursByType(int supervisionHoursType)
    {
        return database.listSupervisionHoursByType(supervisionHoursType);
    }
}
