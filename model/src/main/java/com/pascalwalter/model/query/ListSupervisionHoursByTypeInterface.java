package com.pascalwalter.model.query;

import com.pascalwalter.model.models.SupervisionHour;

import java.util.List;

public interface ListSupervisionHoursByTypeInterface {
    List<SupervisionHour> listSupervisionHoursByType(int supervisionHoursType);
}
