package com.pascalwalter.model.query;

import com.pascalwalter.model.DatabaseHandler;
import com.pascalwalter.model.DatabaseInterface;
import com.pascalwalter.model.models.Patient;

public class UpdatePatientQuery {
    DatabaseInterface database = DatabaseHandler.INSTANCE.getDatabase();

    public Patient updatePatient(Patient patient) {
        return database.updatePatient(patient);
    }
}
