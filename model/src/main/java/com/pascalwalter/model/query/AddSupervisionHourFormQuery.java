package com.pascalwalter.model.query;

import com.pascalwalter.model.DatabaseHandler;
import com.pascalwalter.model.DatabaseInterface;
import com.pascalwalter.model.models.Patient;
import com.pascalwalter.model.models.SupervisionHour;
import com.pascalwalter.model.models.Supervisor;

import java.util.List;

public class AddSupervisionHourFormQuery {
    DatabaseInterface database = DatabaseHandler.INSTANCE.getDatabase();

    public List<Patient> listPatients()
    {
        return database.listPatientsInStatus(Patient.PATIENT_STATE_LAUFEND);
    }

    public List<Supervisor> listSupervisors()
    {
        return database.listSupervisors();
    }

    public SupervisionHour addSupervisionHour(SupervisionHour supervisionHour) {
        return database.addSupervisionHour(supervisionHour);
    }
}
