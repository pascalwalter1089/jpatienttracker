package com.pascalwalter.model.query;

import com.pascalwalter.model.models.Patient;
import com.pascalwalter.model.models.Supervisor;

import java.util.List;

public interface MainFrameQueryInterface {
    List<Patient> listAllPatients();
    List<Supervisor> listAllSupervisors();
}
