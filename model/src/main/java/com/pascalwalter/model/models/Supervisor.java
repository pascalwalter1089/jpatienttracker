package com.pascalwalter.model.models;


import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "supervisors")
public class Supervisor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name", unique = true)
    private String name;

    @OneToMany(mappedBy = "supervisor")
    @OrderBy(value = "date")
    private Set<SupervisionHour> hours;

    public Supervisor() {
    }

    public Set<SupervisionHour> getHours() {
        return hours;
    }

    public void setHours(Set<SupervisionHour> hours) {
        this.hours = hours;
    }

    public Supervisor(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return getName();
    }
}
