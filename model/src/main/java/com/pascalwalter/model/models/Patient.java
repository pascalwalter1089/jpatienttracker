package com.pascalwalter.model.models;

import com.pascalwalter.utils.StringConstants;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "patients")
public class Patient {

    public static final int PATIENT_STATE_LAUFEND = 1;
    public static final int PATIENT_STATE_BEENDET = 2;
    public static final int PATIENT_STATE_PAUSIERT = 3;

    public static final int[] PATIENT_STATES = new int[]{
            Patient.PATIENT_STATE_LAUFEND,
            Patient.PATIENT_STATE_BEENDET,
            Patient.PATIENT_STATE_PAUSIERT
    };

    public static final HashMap<Integer, String> PATIENT_STATES_AS_STRING = new HashMap(){{
        put(Patient.PATIENT_STATE_LAUFEND, StringConstants.PATIENT_STATE_STRING_LAUFEND);
        put(Patient.PATIENT_STATE_BEENDET, StringConstants.PATIENT_STATE_STRING_BEENDET);
        put(Patient.PATIENT_STATE_PAUSIERT, StringConstants.PATIENT_STATE_STRING_PAUSIERT);
    }};

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "hash")
    private String hash;

    @Column(name = "status", nullable = false, columnDefinition = "int default 1")
    private int status;

    @Column(name = "beendet")
    private Date beendet;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true, mappedBy = "patient")
    private Set<PatientHour> hours;

    @ManyToMany(targetEntity = SupervisionHour.class, fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "id")
    private Set<SupervisionHour> supervisionHours;

    public Patient() {
        this.hours = new HashSet<>(0);
        this.supervisionHours = new HashSet<>(0);
        this.status = Patient.PATIENT_STATE_LAUFEND;
    }

    public Patient(String hash) {
        this.hash = hash;
        this.hours = new HashSet<>(0);
        this.supervisionHours = new HashSet<>(0);
        this.status = Patient.PATIENT_STATE_LAUFEND;
    }

    public int getId() {
        return id;
    }

    public Patient setId(int id) {
        this.id = id;

        return this;
    }

    public String getHash() {
        return hash;
    }

    public Patient setHash(String hash) {
        this.hash = hash;

        return this;
    }

    @Override
    public String toString() {
        return getHash();
    }

    public List<PatientHour> getHours() {
        return this.hours.stream().toList();
    }

    public void setHours(HashSet<PatientHour> hours) {
        this.hours = hours;
    }

    public Set<SupervisionHour> getSupervisionHours() {
        return supervisionHours;
    }

    public void setSupervisionHours(Set<SupervisionHour> supervisionHours) {
        this.supervisionHours = supervisionHours;
    }

    public void addSupervisionHour(SupervisionHour hour) {
        this.supervisionHours.add(hour);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
/*        if (!(Collections.singletonList(Patient.PATIENT_STATES).contains(status))) {
            throw new RuntimeException(String.format("State %d is not a valid state", status));
        }*/

        if (status == Patient.PATIENT_STATE_BEENDET) {
            this.beendet = new Date(System.currentTimeMillis());
        }

        this.status = status;
    }

    public String getStatusAsString() {
        return Patient.PATIENT_STATES_AS_STRING.get(this.status);
    }
}
