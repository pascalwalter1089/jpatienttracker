package com.pascalwalter.model.models;

import com.pascalwalter.utils.StringConstants;

import javax.persistence.*;
import java.sql.Date;
import java.util.*;
import java.util.stream.IntStream;

@Entity
@Table(name = "supervision_hours")
public class SupervisionHour {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "type")
    private int type;

    @Column(name = "date")
    private java.sql.Date date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public java.sql.Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Column(name = "duration", nullable = true)
    private Double duration;

    public static final int SUPERVISION_HOUR_TYPE_EINZEL = 0;
    public static final int SUPERVISION_HOUR_TYPE_GROUP = 1;

    public static int[] SUPERVISION_HOUR_TYPES = {
            SUPERVISION_HOUR_TYPE_EINZEL,
            SUPERVISION_HOUR_TYPE_GROUP
    };

    public static HashMap<Integer, String> SUPERVISION_HOURS_TYPES_TO_STRING = new HashMap<>(){{
        put(SUPERVISION_HOUR_TYPE_EINZEL, StringConstants.SUPERVISION_HOUR_EINZEL);
        put(SUPERVISION_HOUR_TYPE_GROUP, StringConstants.SUPERVISION_HOUR_GROUP);
    }};

    @ManyToMany(mappedBy = "supervisionHours")
    private Set<Patient> patients;

    @ManyToOne(targetEntity = Supervisor.class, cascade = CascadeType.PERSIST)
    private Supervisor supervisor;

    public SupervisionHour() {

    }

    public SupervisionHour(Double duration, Set<Patient> patients, Supervisor supervisor, int type) {
        this.duration = duration;
        this.patients = patients;
        this.supervisor = supervisor;
        this.type = type;
    }

    public static int stringTypeToInt(String typeAsString) {
        if (typeAsString.equals(StringConstants.SUPERVISION_HOUR_EINZEL)) {
            return 0;
        }

        if (typeAsString.equals(StringConstants.SUPERVISION_HOUR_GROUP)) {
            return 1;
        }

        throw new IllegalArgumentException();
    }

    public void setType(int type) {
        if (!IntStream.of(SUPERVISION_HOUR_TYPES).anyMatch(x -> x == type)) {
            throw new RuntimeException("Type not valid!");
        }
        this.type = type;
    }

    public List<Patient> getPatients() {
        return patients.stream().toList();
    }

    public Supervisor getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(Supervisor supervisor) {
        supervisor.getHours().add(this);
        this.supervisor = supervisor;
    }

    public void setPatients(Patient[] patients) {
        this.patients = new HashSet<>(Arrays.asList(patients));
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

    public double getDuration() {
        return this.duration;
    }
}
