package com.pascalwalter.model.models;

import com.pascalwalter.utils.StringConstants;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collections;
import java.util.HashMap;

@Entity
@Table(name="patient_hours")
public class PatientHour extends Hour {
    public static int PATIENT_HOUR_TYPE_PROBATORIK = 2;
    public static int PATIENT_HOUR_TYPE_KZT1 = 3;
    public static int PATIENT_HOUR_TYPE_KZT2 = 4;
    public static int PATIENT_HOUR_TYPE_LZT = 5;
    private static final int PATIENT_HOUR_TESTUNG = 6;
    private static final int PATIENT_HOUR_PSYCHOTHERAPEUTISCHE_SPRECHSTUNDE = 7;

    public static int[] PATIENT_HOUR_TYPES_PATIENT = {
            PATIENT_HOUR_TYPE_PROBATORIK,
            PATIENT_HOUR_TYPE_KZT1,
            PATIENT_HOUR_TYPE_KZT2,
            PATIENT_HOUR_TYPE_LZT,
            PATIENT_HOUR_TESTUNG,
            PATIENT_HOUR_PSYCHOTHERAPEUTISCHE_SPRECHSTUNDE
    };

    public static int[] PATIENT_HOUR_TYPES_BEZUGSPERSON = {
            PATIENT_HOUR_TYPE_PROBATORIK,
            PATIENT_HOUR_TYPE_KZT1,
            PATIENT_HOUR_TYPE_KZT2,
            PATIENT_HOUR_TYPE_LZT,
            PATIENT_HOUR_PSYCHOTHERAPEUTISCHE_SPRECHSTUNDE
    };

    public static HashMap<Integer, String> PATIENT_HOURS_TYPES_TO_STRING = new HashMap<>(){{
        put(PATIENT_HOUR_TYPE_PROBATORIK, StringConstants.PATIENT_HOUR_TYPE_PROBATORIK);
        put(PATIENT_HOUR_TYPE_KZT1, StringConstants.PATIENT_HOUR_TYPE_KZT1);
        put(PATIENT_HOUR_TYPE_KZT2, StringConstants.PATIENT_HOUR_TYPE_KZT2);
        put(PATIENT_HOUR_TYPE_LZT, StringConstants.PATIENT_HOUR_TYPE_LZT);
        put(PATIENT_HOUR_PSYCHOTHERAPEUTISCHE_SPRECHSTUNDE, StringConstants.PATIENT_HOUR_TYPE_PTS);
        put(PATIENT_HOUR_TESTUNG, StringConstants.PATIENT_HOUR_TYPE_TESTUNG);
    }};

    public static HashMap<String, Integer> PATIENT_HOURS_STRING_TO_TYPES = new HashMap<>(){{
        put(StringConstants.PATIENT_HOUR_TYPE_PROBATORIK, PATIENT_HOUR_TYPE_PROBATORIK);
        put(StringConstants.PATIENT_HOUR_TYPE_KZT1, PATIENT_HOUR_TYPE_KZT1);
        put(StringConstants.PATIENT_HOUR_TYPE_KZT2, PATIENT_HOUR_TYPE_KZT2);
        put(StringConstants.PATIENT_HOUR_TYPE_LZT, PATIENT_HOUR_TYPE_LZT);
        put(StringConstants.PATIENT_HOUR_TYPE_PTS, PATIENT_HOUR_PSYCHOTHERAPEUTISCHE_SPRECHSTUNDE);
        put(StringConstants.PATIENT_HOUR_TYPE_TESTUNG, PATIENT_HOUR_TESTUNG);
    }};

    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private Patient patient;

    @Column(name = "recipient")
    @Enumerated(EnumType.STRING)
    private PatientHourRecipient recipient;

    public PatientHour() {
    }

    public PatientHour(Patient patient, int type, PatientHourRecipient recipient, Date date) {
        this.patient = patient;
        this.type = type;
        this.recipient = recipient;
        this.date = date;
    }

    public void setType(int type, PatientHourRecipient recipient) {
        if (recipient.equals(PatientHourRecipient.PATIENT) && Collections.singletonList(PATIENT_HOUR_TYPES_PATIENT).contains(type)) {
            this.type = type;
            this.recipient = recipient;

            return;
        }

        if (recipient.equals(PatientHourRecipient.BEZUGSPERSON) && Collections.singletonList(PATIENT_HOUR_TYPES_BEZUGSPERSON).contains(type)) {
            this.type = type;
            this.recipient = recipient;

            return;
        }

        throw new RuntimeException();
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getRecipient() {
        return this.recipient.name();
    }

    public enum PatientHourRecipient {
        PATIENT,
        BEZUGSPERSON,
    }
}
