package com.pascalwalter.model;

import java.util.Map;

public enum DatabaseHandler {
    INSTANCE;

    private DatabaseInterface database;

    public void init(Map<String, String> properties) {
        if (null != this.database) {
            return;
        }

        this.database = new H2DatabaseHandler(properties);
    }

    public boolean isConnected() {
        if (this.database == null) {
            return false;
        }

        return database.isConnected();
    }

    public DatabaseInterface getDatabase() {
        if (null == this.database) {
            throw new RuntimeException("Database not initialized");
        }

        return database;
    }
}
