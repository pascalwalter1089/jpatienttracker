package com.pascalwalter.model;

import com.pascalwalter.model.models.*;

import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class H2DatabaseHandler implements DatabaseInterface {
    private final EntityManager entityManager;

    public H2DatabaseHandler() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("com.pascalwalter.jpa");
        entityManager = factory.createEntityManager();
    }

    public H2DatabaseHandler(Map<String, String> properties) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("com.pascalwalter.jpa", properties);
        entityManager = factory.createEntityManager();
    }

    @Override
    public User getUser(String username, String password) {
        return null;
    }

    @Override
    public User addUser(User user) {
        entityManager.persist(user);
        entityManager.refresh(user);

        return user;
    }

    @Override
    public Supervisor getSupervisor(int id) {
        return null;
    }

    @Override
    public Supervisor addSupervisor(Supervisor supervisor) {
        entityManager.getTransaction().begin();
        entityManager.persist(supervisor);
        entityManager.getTransaction().commit();
        entityManager.refresh(supervisor);
        return supervisor;
    }

    @Override
    public List<Supervisor> listSupervisors() {
        return entityManager.createQuery("SELECT a FROM Supervisor a", Supervisor.class).getResultList();
    }

    @Override
    public Patient getPatient(int id) {
        return null;
    }

    @Override
    public Patient addPatient(Patient patient) {
        entityManager.getTransaction().begin();
        entityManager.persist(patient);
        entityManager.getTransaction().commit();
        entityManager.refresh(patient);
        return patient;
    }

    @Override
    public List<Patient> listPatients() {
        return entityManager.createQuery("SELECT a FROM Patient a", Patient.class).getResultList();
    }

    @Override
    public PatientHour getPatientHour(int id) {
        return null;
    }

    @Override
    public PatientHour addPatientHour(PatientHour patientHour) {
        entityManager.getTransaction().begin();
        entityManager.persist(patientHour);
        entityManager.getTransaction().commit();
        entityManager.refresh(patientHour);
        return patientHour;
    }

    @Override
    public List<PatientHour> listPatientHours() {
        return null;
    }

    @Override
    public List<PatientHour> listPatientHoursByPatient(int patientId) {
        return null;
    }

    @Override
    public List<PatientHour> listPatientHoursByType(int patientHourType) {
        return null;
    }

    @Override
    public SupervisionHour getSupervisionHour(int id) {
        return null;
    }

    @Override
    public SupervisionHour addSupervisionHour(SupervisionHour supervisionHour) {
        entityManager.getTransaction().begin();
        entityManager.persist(supervisionHour);
        entityManager.getTransaction().commit();
        entityManager.refresh(supervisionHour);
        return supervisionHour;
    }

    @Override
    public List<SupervisionHour> listSupervisionHour() {
        return entityManager.createQuery("SELECT a FROM SupervisionHour a", SupervisionHour.class).getResultList();
    }

    @Override
    public List<SupervisionHour> listSupervisionHoursByPatient(Patient patient) {
        return entityManager.createQuery("SELECT a FROM SupervisionHour a", SupervisionHour.class).getResultList();
    }

    @Override
    public List<SupervisionHour> listSupervisionHoursBySupervisor(int supervisorId) {
        return null;
    }

    @Override
    public List<SupervisionHour> listSupervisionHoursByType(int supervisionHoursType) {
        return entityManager.createQuery("SELECT a FROM SupervisionHour a WHERE a.type = :supervisionHourType", SupervisionHour.class)
                .setParameter("supervisionHourType", supervisionHoursType)
                .getResultList();
    }

    @Override
    public List<SupervisionHour> listSupervisionHoursBySupervisorAndType(Supervisor supervisor, int supervisionHoursType) {
        return entityManager.createQuery("SELECT a FROM SupervisionHour a WHERE a.type = :supervisionHourType AND a.supervisor = :supervisor", SupervisionHour.class)
                .setParameter("supervisionHourType", supervisionHoursType)
                .setParameter("supervisor", supervisor)
                .getResultList();
    }

    @Override
    public List<Patient> listPatientsInStatus(int status) {
        return entityManager.createQuery("SELECT a FROM Patient a WHERE a.status = :status", Patient.class)
                .setParameter("status", status)
                .getResultList();
    }

    public Patient updatePatient(Patient patient) {
        entityManager.getTransaction().begin();
        entityManager.persist(patient);
        entityManager.getTransaction().commit();
        entityManager.refresh(patient);
        return patient;
    }

    @Override
    public boolean isConnected() {
        return entityManager != null && entityManager.isOpen();
    }

    @Override
    public void removePatient(Patient patient) {
        entityManager.getTransaction().begin();
        entityManager.remove(patient);
        entityManager.getTransaction().commit();
    }
}
